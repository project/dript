<?php
/**
 *	Lisp interpreter for server-side casual scripting
 *	By Abdullah "chelah" Daud
 *	18 December 2006
 *	mailto:chelah@bootbox.net
 *	http://chelah.cara-cerna.com
 *
 *	Why another lisp interpreter?
 *	1. The initial objective is to provide a casual scripting language
 *	   for Drupal an open source content management platform (http://drupal.org)
 *	   to ride on top of the CCK module. There is a Computed Field module for 
 *	   CCK but it uses PHP script which is not so casual.
 *	   a. The script will be available through Filter and yet another computed 
 *	      field.
 *	   b. Each function can be categorized and the categories can be controlled 
 *	      through Access Control.
 *	   c. The scripting will be safe and non-intrusive like javascript.
 *	2. Casual programming is for non-programmer but who knows enough about 
 *	   coding like spreadsheet macro coding. The code is usually short and once 
 *	   done will be forgotten. Serious programmers should stick to PHP.
 *	3. Each evaluation will be timed. An execution that takes a longer time will 
 *	   be automatically terminated. This is the correct behavior for server-side 
 *	   scripting. This also will void any garbage collection requirement since 
 *	   all resources will be released once an evaluation exit.
 *	4. Each function is implemented separately as an add-on function module so 
 *	   that the core interpreter will be small and load faster. This will make 
 *	   this interpreter extensible. A function module will only be loaded once 
 *	   the function is called.
 *	5. Pure lisp implementation with its simplest form and behavior. Not like 
 *	   CommonLisp.
 *	   a. A form is made up of an element, a list of elements, or nil. The word 
 *	      'nil' is an alias for empty form or (). Empty form is an important 
 *	      feature in lisp. A form is enclosed with an opening '(' and a closing 
 *        ')' parenteses.
 *	   b. An element is a word, a string, or another form.
 *	   c. A word is made up of a character or a series of characters except
 *	      white spaces and special characters.
 *	   d. Special characters are (, ), ', ", and ;.
 *	   e. A string is anything in between two double quotes (") in a single 
 *	      line. A string may include escape characters that start with backslash 
 *	      (\): \", \t, \n, \r.
 *	   f. An element can be marked as quoted with single mark (').
 *	      A quoted element will not be evaluated by the evaluator.
 *	   g. A form can be evaluated to become another form or nil. (In general 
 *	      programming term: a form once evaluated will return another form or 
 *	      nil).
 *	   h. Evaluation of a form:
 *	      a. The evaluator will first assume the first element as a function 
 *	         name and check with the function database. If the function exist 
 *	         than, first, if the function wants arguments to be evaluated than 
 *	         the evaluator will evaluate the rest of the elements, and second, 
 *		     pass the elements to the function for further evaluation. A 
 *		     function will return a form, an element or nothing. The evaluator 
 *		     will return whatever the function returns. But if the function
 *	         return nothing than the evaluator will return nil as a marker.
 *	         Some special functions (like lambda function) will request the 
 *		     evaluator to reevaluate the resulting form after the function 
 *		     return. This is lisp's very powerful feature and more together with 
 *		     quote.
 *	      b. Else if the function does not exist than the evaluator will 
 *		     evaluate all elements and return them in another form.
 *	   i. Evaluation of an element:
 *	      a. If the element is quoted then the evaluator will return the element 
 *		     with one quote removed.
 *	      b. Else if the element is a string than the evaluator will return it 
 *		     as is with no alteration whatsoever.
 *	      c. Else if the element is a word than the evaluator will check with 
 *		     the variable database and if a variable with the same name with the 
 *		     word exist than the evaluator will return the value of the variable 
 *		     otherwise the evaluator will return the word as is.
 *	      d. Else if the element is a form than the evaluator will evaluate the 
 *		     form as in (h).
 *
 *	Quote ('):
 *		Quote (') is not comment. Quote is a flag for the interpreter
 *		againts an element whether or not to evaluate it. When the
 *		interpreter come accros an element with quotes it will strip
 *		one quote from the element and will not attempt to evaluate
 *		the element. However function that take quoted element may
 *		choose to ignore the quotes and/or manipulate them. A function 
 *		may also quote its return values.
 *
 *	nil - not in list:
 *		nothing,
 * 		(),
 *		(()), (()()) ...
 *		nil,
 *		(nil), (nil ...), ((nil)) ...
 *		(nil ()) ...
 *		or any form that contains only () an/or nil.
 *		'nil => nil, '() => (). 
 *
 *	T - not nil
 *		T is not necessarily mean 'true' but many boolean-like
 *		operations may treat it so. Lisp do not have the concept
 *		of true and false. In lisp it is either 'something' or
 *		'nothing'. Both 'something' and 'nothing' are represented 
 *		by forms. In lisp 'T' is more to refer to 'something' and
 *      'nil' to 'nothing'.
 *
 *	Form element array structure:
 *
 *		[0]		=> element type
 *		[1]		=> element attribute
 *		[1][0]	=> element quote count
 *		[1][1]	=? element row
 *		[1][2]	=> element column
 *		[2]		=> element
 *
 *		If [0] => type form
 *			then [2..n] => point to other element array structure
 *
 *		If [0] => type string
 *			then [2] => unquoted string
 *
 *		If [0] => type word
 *			then [2] => word
 */
 
include_once 'lisp_parser.inc';
class lisp extends lisp_parser
{
	// Maximum time allowed for the execution of an evaluation
	var $execution_time_frame = 10; // in seconds
	// Execution start time
	var $execution_start_time = 0;
	// Execution exit code applied on function is_black_flagged.
	// If the code is not zero than it was black_flagged.
	var $execution_exit_code = 0;

	var $cur_path = ""; // current path
	var $user_path = ""; // user path (not relative to current path)
	var $ext_funs = array(); // external function cache
	var $func_aliases = array();
	var $lambda_prefix = "__lambda_";

	// Evaluation states
	var $namespaces = array(); // active namespaces
	var $global_vars = array(); // global variables
	var $local_vars = array(); // local variables
	var $is_var_scope_local = false;
	var $lambda_id = 0; // current lambda id for nested lambda functions
	var $defuns = array(); // user defined functions

	// Variables used by eval_text()
	var $output_text = ""; // Appended by lisp_print() (called by (print 'msg))
	var $start_tag = "[lisp";
	var $end_tag = "/]";
	var $after_eval_text_func = null;
	
	var $loaded_files = array(); // (load-once) filename cache
	
	function init_lisp()
	{
		$this->init_lisp_parser();
	}
	
	/**
	 * Load function name aliases
	 */
	function load_aliases()
	{
		include_once($this->cur_path . "alias.inc");
		$this->func_aliases = lisp_get_function_aliases();
	}

	/**
	 * Initialise
	 */	
	function init($path, $user_path)
	{
		$this->init_lisp();
		$this->cur_path = $path;
		$this->user_path = $user_path;
		$this->load_aliases();
	}

	function dump($line)
	{
		$filename = $this->user_path . "dump.txt";
		
   		if (!$handle = fopen($filename, 'a+'))
			return; 

   		fwrite($handle, $line . "\r\n"); 
   		fclose($handle);
	}
	
	/**
	 * Get evaluation states
	 */	
	function get_eval_states()
	{
		return array($this->global_vars, 
								 $this->local_vars, 
								 $this->lambda_id,
								 $this->defuns,
								 $this->output_text,
								 $this->after_eval_text_func);	
	}

	/**
	 * Set evaluation states
	 */
	function set_eval_states($stt)
	{
		$this->global_vars = $stt[0]; 
		$this->local_vars = $stt[1]; 
		$this->lambda_id = $stt[2];
		$this->defuns = $stt[3];
		$this->output_text = $stt[4];
		$this->after_eval_text_func = $stt[5];	
	}

	/**
	 * Clear evaluation states
	 */
	function clear_eval_states()
	{
		$this->global_vars = array(); 
		$this->local_vars = array(); 
		$this->lambda_id = 0;
		$this->defuns = array();
		$this->output_text = "";
		$this->after_eval_text_func = null;	
	}

	/**
	 * Translate
	 */
	function t($str, $args = null)
	{
		// TODO: replace $str with the current language
		
		if (is_array($args))
		{
			foreach ($args as $key => $val)
			{
				$str = str_replace($key, $val, $str);
			}
		}
		
		return $str;
	}
	
	/**
	 * Start execution timing
	 */
	function go()
	{
		$this->execution_start_time = time();
		$this->execution_exit_code = 0;
	}

	/**
	 * Check if the execution is within the time frame
	 */
	function is_black_flagged($exit_code = 0)
	{
		$ct = time();

		if (($ct - $this->execution_start_time) > $this->execution_time_frame)
		{
			if ($exit_code != 0)
				$this->execution_exit_code = $exit_code;

			return true;
		}

		return false;
	}

	/**
	 * Check if the name is a reserved keyword
	 */
	function is_reserved_keyword($name)
	{
		$lname = strtolower($name);
		
		if ($lname == "nil" || $lname == "T")
			return true;
			
		return false;
	}

	/**
	 * Begin a namespace
	 */
	function ns_begin($ns)
	{
		array_unshift($this->namespaces, $ns);	
	}
	
	/**
	 * End a namespace
	 */
	function ns_end($ns)
	{
		$rmv = array();
		$rmv_ns = array_shift($this->namespaces);
		
		while ($rmv_ns != $ns)
		{
			array_unshift($rmv, $rmv_ns);
			$rmv_ns = array_shift($this->namespaces);
		}
			
		foreach ($rmv as $val)
			array_unshift($this->namespaces, $val);
	}
	
	/**
	 * Set global variable
	 */
	function set_global_var($name, $type, $quotes, $value)
	{
		if ($this->is_reserved_keyword($name))
			return;
			
		$name = str_replace("-", "_", $name);		
		$this->global_vars[$name] = array($type, $quotes, $value);
	}

	/**
	 * Get the global variable $name.
	 * @return
	 * The variable value or null.
	 */
	function get_global_var($name)
	{
		if ($this->is_reserved_keyword($name))
			return;
			
		$name = str_replace("-", "_", $name);
		
		if (array_key_exists($name, $this->global_vars))
			return $this->global_vars[$name];
	}
	
	/**
	 * Set local variable
	 */
	function set_local_var($name, $type, $quotes, $value)
	{
		if ($this->is_reserved_keyword($name))
			return;
			
		$name = str_replace("-", "_", $name);		
		$this->local_vars[$name] = array($type, $quotes, $value);
	}

	/**
	 * Get the local variable $name.
	 * @return
	 * The variable value or null.
	 */
	function get_local_var($name)
	{
		if ($this->is_reserved_keyword($name))
			return;
			
		$name = str_replace("-", "_", $name);
		
		if (array_key_exists($name, $this->local_vars))
			return $this->local_vars[$name];
	}
	
	/**
	 * Set variable
	 */
	function set_var($name, $type, $quotes, $value)
	{
		if ($this->is_reserved_keyword($name))
			return;
			
		$name = str_replace("-", "_", $name);
		
		if ($this->is_var_scope_local)
			$this->local_vars[$name] = array($type, $quotes, $value);
		else
			$this->global_vars[$name] = array($type, $quotes, $value);
	}

	/**
	 * Get the variable $name, local first.
	 * If local does not exist than get global.
	 * @return
	 * The variable value or null.
	 */
	function get_var($name, &$global)
	{
		$global = false;
		
		if ($this->is_reserved_keyword($name))
			return;
			
		$name = str_replace("-", "_", $name);
		
		if (array_key_exists($name, $this->local_vars))
		{
			return $this->local_vars[$name];
			//return array($var[0], $var[1], $var[2]);
		}
		
		if (array_key_exists($name, $this->global_vars))
		{
			$global = true;
			return $this->global_vars[$name];
		}
	}
	
	/**
	 * Get function alias or just return the name
	 */
	function get_func_alias($name)
	{
		if ($this->is_reserved_keyword($name))
			return;
			
		$key = array_search($name, $this->func_aliases);
		
		if ($key)
			return $key;
			
		return $name;
	}

	/**
	 * Check whether the $name is a function
	 * Return as reference the full name with padded namespace
	 */	
	function is_func($name, &$ns_name)
	{
		$ns_name = $name;
		
		if ($this->is_reserved_keyword($name))
			return false;
			
		if (array_key_exists($name, $this->func_aliases))
			$name = $this->func_aliases[$name];

		$name = str_replace("-", "_", $name);
		
		if (is_file($this->cur_path . "func/$name.inc"))
			return true;

		foreach($this->namespaces as $ns)
		{
			$nsn = $ns . '_' . $name;
			
			if (is_file($this->cur_path . "func/$nsn.inc"))
			{
				$ns_name = $nsn;
				return true;
			}	
		}
		
		return false;
	}

	/**
	 * Get the $name function object.
	 * Use the one available in the cache, or
	 * load form the function file.
	 */
	function get_func($name)
	{
		if ($this->is_reserved_keyword($name))
			return;
			
		if (array_key_exists($name, $this->func_aliases))
			$name = $this->func_aliases[$name];

		$name = str_replace("-", "_", $name);

		if (array_key_exists($name, $this->ext_funs))
			return $this->ext_funs[$name];

		include_once($this->cur_path . "func/$name.inc");
		$this->ext_funs[$name] = call_user_func("create_lisp_func_" . $name);

		return $this->ext_funs[$name];
	}

	/**
	 * Get function special attribute.
	 * Some function object may not define certain attributes so
	 * defaults may be returned.
	 * The return type may vary.
	 */
	function func_special_attr($func, $attr)
	{
		switch ($attr)
		{
			case "dont_want_nil":

				if (!method_exists($func, $attr))
					return false;

				return $func->dont_want_nil($this);

			case "dont_eval_args":

				if (!method_exists($func, $attr))
					return false;

				return $func->dont_eval_args($this);

			case "want_re_eval":

				if (!method_exists($func, $attr))
					return false;

				return $func->want_re_eval($this);

			case "name":

				if (!method_exists($func, $attr))
					return false;

				return $func->name($this);

			case "syntax":

				if (!method_exists($func, $attr))
					return false;

				return $func->syntax($this);

			case "description":

				if (!method_exists($func, $attr))
					return false;

				return $func->description($this);

		}
	}

	/**
	 * User define a new lisp function
	 */
	function defun(&$name, &$arg_vars, &$body, &$source)
	{
		$this->defuns[$name] = array($arg_vars, $body, $source);
	}
	
	/**
	 * User define a lambda function
	 */
	function defun_lambda(&$arg_vars, &$body, &$source)
	{
		$id = $this->lambda_id;
		++$this->lambda_id;
		$name = $this->lambda_prefix . $id;
		$this->defun($name, $arg_vars, $body, $source);
		
		return $name;
	}

	/**
	 * Check if $name is a defined lambda function
	 */
	function is_lambda($name)
	{
		$plen = strlen($this->lambda_prefix);
		$pre = substr($name, 0, $plen);
		
		if ($pre == $this->lambda_prefix)
		{
			$post = substr($name, $plen);
			
			if (ctype_digit($post))
			{
				$id = (int)$post;
						
				if ($id < $this->lambda_id)
					return true;
			}
		}
			
		return false;
	}
	
	/**
	 * Undefined a lambda function
	 */
	function undefun_lambda($name)
	{
		if (!$this->is_lambda($name))
			return;
		
		$this->defuns[$name] = null;
		
		$plen = strlen($this->lambda_prefix);
		$id = (int)substr($name, $plen);

		if ($id == ($this->lambda_id - 1))
			$this->lambda_id = $id;		
	}
		
	/**
	 * Check if name is a user define function name
	 */
	function is_defun($name)
	{
		if (array_key_exists($name, $this->defuns))
		{
			$fn = $this->defuns[$name];
		
			if (is_array($fn))
				return true;
		}
			
		return false;
	}
				
	/**
	 * Get all function names into an array
	 * including system and user defined
	 */
	function get_all_func()
	{
		$funs = array();
		
		if ($handle = opendir($this->cur_path . "func")) 
		{
    		while (false !== ($file = readdir($handle))) 
			{
        		if ($file != "." && $file != "..") 
				{
            		if (strstr($file, ".inc"))
            		{
						$file = str_replace("_", "-", $file);
						$file = str_replace(".inc", "", $file);
            			$funs[] = $file;
            		}
        		}
    		}
    	
    		closedir($handle);
		}		
		
		foreach($this->defuns as $key => $val)
			$fun[] = $key;
		
		return $funs;
	}
	
	/**
	 * Check if $ele has a structure of a lisp element array
	 */
	function is_ele(&$ele)
	{
		if (!is_array($ele))
			return false;
			
		$cnt = count($ele);
		
		if ($cnt < 2)
			return false;
			
		$attr = $ele[1];
		
		if (!is_array($attr))
			return false;
			
		$cnt = count($attr);
		
		if ($cnt < 3)
			return false;
			
		return true;	
	}
	
	/**
	 * Evaluate a user defined function
	 */
	function eval_defun(&$name, &$args)
	{
		if (!array_key_exists($name, $this->defuns))
			return;
			
		$fn = $this->defuns[$name];
		
		if (!is_array($fn))
			return;
			
		$cnt = count($fn);
		
		if ($cnt < 3)
			return;
		
		$arg_vars = $fn[0];
		$body = $fn[1];
		$source = $fn[2];
		
		// Push local variable table
		$xlvars = $this->local_vars;
		$this->local_vars = array();
		
		// Set argument variables
		if (is_array($arg_vars))
		{
			$cnt = count($arg_vars);
			
			for($i = 0; $i < $cnt; $i++)
			{
				$carg = $args[$i + 3]; // After type, attr and fname
				
				if ($this->is_ele($carg))
					$this->set_local_var($arg_vars[$i], 
										 $carg[0], $carg[1][0], $carg[2]);
				else // undefine arguments are set to nils
					$this->set_local_var($arg_vars[$i], lisp_eletype_word, 0, "nil");
			}
		}		
		
		// Evaluate body
		if (is_array($body))
		{
			foreach($body as $key => $expr)
			{
				if ($key > 1)
				{
					$cur = $this->cur_get();
					$expr_str = $this->form_to_string($expr);
					$rst = $this->evaluate($expr_str, $source, $dre);
					$this->cur_set($cur);
		
					if ($this->is_err())
					{
						$this->local_vars = $xlvars; // Pop local variable table
						$this->undefun_lambda($name);
						return;
					}
				}
			}
		}
		
		$this->local_vars = $xlvars; // Pop local variable table
		$this->undefun_lambda($name);
			
		return $rst[2];
	}
	
	/**
	 * Evaluate the given expression $expr string
	 * and return the result form as array
	 */
	function evaluate($expr, $source, &$do_re_eval)
	{
		$do_re_eval = false;
		$this->cerr();
		$form = array(lisp_eletype_form, array(0, $this->cur_row, $this->cur_column));
		$len = strlen($expr);
		$first = true;
		$do_eval = true;
		$want_nil = true;
		$is_func = false;
		$is_defun = false;
		$defun = "";
		$func = null;
		$re_eval = false;
		
		while ($len > 0)
		{
			if ($this->is_black_flagged(-1))
			{
				$this->serr(lisp_err_black_flagged, 
							$this->cur_row, 
							$this->cur_column, 
							$source);
				return;
			}

			$expr = $this->skip_element($expr, $ele, $type, $qcnt, $row, $col);

			if ($this->is_err())
				return;

			if ($qcnt > 0)
			{
				if ($do_eval)
					--$qcnt;

				switch ($type)
				{
					case lisp_eletype_form:
						$form[] = array(lisp_eletype_raw_form, 
										array($qcnt, $row, $col), 
										"(" . $ele . ")");
						break;

					case lisp_eletype_string:
						$form[] = array(lisp_eletype_raw_string, 
										array($qcnt, $row, $col), $ele);
						break;

					case lisp_eletype_word:
						$form[] = array(lisp_eletype_raw_word, 
										array($qcnt, $row, $col), $ele);
						break;
				}
			}
			else
			{
				switch ($type)
				{
					case lisp_eletype_form:

						if ($do_eval)
						{
							$cur = $this->cur_get();
							$rst = $this->evaluate($ele, $source, $dre);
							$this->cur_set($cur);
							
							if ($this->is_err())
								return;

							if ($dre && $first)
								$re_eval = true;
								
							if ($rst)
								$form[] = $rst;
						}
						else
							$form[] = array(lisp_eletype_raw_form, 
											array(0, $row, $col), 
											"(" . $ele . ")");

						break;

					case lisp_eletype_string:
					
						if ($do_eval)
							$form[] = array($type, array(0, $row, $col), $ele);
						else	
							$form[] = array(lisp_eletype_raw_string, 
											array(0, $row, $col), $ele);

						break;

					case lisp_eletype_word:

						$skip_do_eval = false;

						if ($first)
						{
							if ($this->is_defun($ele))
							{
								$is_defun = true;
								$form[] = array($type, array(0, $row, $col), $ele);
								$defun = $ele;
								$skip_do_eval = true;
							}
							else if ($this->is_func($ele, $ns_name))
							{
								$is_func = true;
								$form[] = array($type, array(0, $row, $col), $ns_name);
								$func = $this->get_func($ns_name);
								$do_eval = !$this->func_special_attr($func, "dont_eval_args");
								$want_nil = !$this->func_special_attr($func, "dont_want_nil");
								$do_re_eval = $this->func_special_attr($func, "want_re_eval");
								$skip_do_eval = true;
							}
						}

						if (!$skip_do_eval)
						{
							if ($do_eval)
							{
								$var = $this->get_var($ele, $g);
							
								if ($var)
									$form[] = array($var[0], 
													array($var[1], -1, -1), 
													$var[2]);
								else
									$form[] = array($type, 
													array(0, $row, $col), 
													$ele);
							}
							else
								$form[] = array(lisp_eletype_raw_word, 
												array(0, $row, $col), $ele);
						}

						break;
				}
			}

			$len = strlen($expr);
			$first = false;
		}

		if ($is_defun)
		{
			$xvs = $is_var_scope_local;
			$is_var_scope_local = true;
			$rst = $this->eval_defun($defun, $form);
			$is_var_scope_local = $xvs;
			return $rst;
		}
		
		if ($is_func && is_object($func))
		{
			$rst = $func->evaluate($this, $form, $source);
			
			if ($rst)
				return $rst;
			
			if ($want_nil)
				return array(lisp_eletype_word, array(0, -1, -1), "nil");
					
			return; // return nothing				
		}

		if ($re_eval)
		{
			$rexpr = $this->form_to_string($form);
			$rst = $this->evaluate($rexpr, $source, $dre);
			
			if ($this->is_err())
				return;
				
			return $rst[2]; // Only the content
		}
		
		return $form;
	}

	/**
	 * Evaluate the given expression $expr string
	 * and return the result of the last expression
	 * as string.
	 */
	function evaluate_expr($expr, $source)
	{
		$rst = $this->evaluate_expr($expr, $source, $dre);
		
		if ($this->is_err())
			return err_str();
			
		$ele = last_ele($rst);
		return $this->form_to_string($ele);	
	}

	/**
	 * Test if the form is nil:
	 * {nothing}, nil, [(), (()), ...}, [(nil), ((nil)), ...] or
	 * any form that contains only nils.
	 * @return
	 * TRUE if the form is nil
	 */
	function is_nil($form)
	{
		if ($form == null)
			return true;
			
		$form = $this->form_to_string($form);
		$form = str_replace("'", "", $form); // remove quotes
		$form = strtolower($form);
		$form = $this->skip_spaces($form);
				
		$len = strlen($form);
		
		while ($len > 0)
		{
			$cur = $this->cur_get();
			$form = $this->skip_element($form, $ele, $type, $qcnt, $row, $col);
			$this->cur_set($cur);
					
			if ($this->is_err())
				return false;
					
			if (strlen($ele) > 0)
			{
				if ($type == lisp_eletype_form)
				{
					if (!$this->is_nil($ele))
						return false;
				}
				else if ($type == lisp_eletype_word)
				{
					if (strlen($ele) > 0 && $ele != 'nil')
						return false;
				}
				else
					return false;
			}	
					
			$len = strlen($form);
		}
					
		return true;
	}
	
	/**
	 * Read the last element of the form.
	 * If the form is an element then return the form.
	 * Otherwise return nothing.
	 */
	function last_ele($form)
	{
		if ($form == null)
			return;
			
		if (is_array($form))
		{
			if ($form[0] == lisp_eletype_form)
			{
				$cnt = count($form);
				
				if ($cnt > 2)
					return $form[$cnt - 1]; 
			}
		}
			
		return $form;
	}
	
	/**
	 * Return a count of quote marks as a string
	 */
	function quote_str($count)
	{
		$qstr = "";

		for ($i = 0; $i < $count; $i++)
			$qstr .= "'";

		return $qstr;
	}

	/**
	 * Convert evaluator's form array to string
	 */
	function form_to_string($form)
	{
		if ($form == null)
			return;
			
		if (!is_array($form))
			return $form;
			
		$str = "";
		$qcnt = 0;
		$type = 0;

		foreach ($form as $index => $ele)
		{
			if ($index == 0)
			{
				$type = $ele;
				continue;
			}

			if ($index == 1)
			{
				$qcnt = $ele[0];
				continue;
			}

			if (strlen($str) <= 0)
			{
				$str .= $this->quote_str($qcnt);

				if ($type == lisp_eletype_form)
					$str .= "(";
			}
			else
			{
				$str .= " ";
				$str .= $this->quote_str($qcnt);
			}

			if ($type == lisp_eletype_form)
				$str .= $this->form_to_string($ele);
			else if ($type == lisp_eletype_string || 
					 $type == lisp_eletype_raw_string)
				$str .= "\"" . $ele . "\"";
			else
				$str .= $ele;
		}

		if (strlen($str) > 0)
		{
			if ($type == lisp_eletype_form)
				$str .= ")";
		}
		else
			$str .= "nil";

		return $str;
	}
	
	/**
	 * Remove the beginning and ending double quotes.
	 * @RETURN
	 * The string with both double quote ends stripped.
	 */
	function strip_dquotes($str)
	{
		if ($str[0] != "\"")
			return $str;
		
		$cur = $this->cur_get();
		$this->skip_string($str, $sstr, $trun);
		$this->cur_set($cur);
		return $sstr;
	}
	
	/**
	 * Print to the output.
	 * Used by external lisp function print()
	 * in conjunction with eval_text().
	 */
	function lisp_print($str)
	{
		$this->output_text .= $str;
	}
		 
	/**
	 * Get error string.
	 */
	function err_str()
	{
		return $this->t("{Error %code at [%row,%col] in %source}",
			  		   array("%code" => $this->gerr_str($this->error_code), 
						     "%row" => $this->error_row,
							 "%col" => $this->error_column,
							 "%source" => $this->error_source));
	}
	
	/**
	 * Print error to the output.
	 * Used by eval_text().
	 */
	function perr()
	{
		$this->output_text .= $this->err_str();
	}
	
	/**
	 * Skip enclosing tags and extract the script.
	 * Return the string after the tags.
	 */
	function skip_tags($str, &$script, &$print_rst, &$has_tags)
	{
		$script = "";
		$print_rst = false;
		$has_tags = false;
		$start_tag_len = strlen($this->start_tag);
		$end_tag_len = strlen($this->end_tag);
		
		$tag = substr($str, 0, $start_tag_len);
			
		if ($tag != $this->start_tag) // No lisp script tag
			return $str;
			
		$str = substr($str, $start_tag_len); // skip start tag
		$has_tags = true;	
		
		if ($str[0] == 'e') 
		{
			$str = substr($str, 1); // skip 'evaluate' marker
			$print_rst = true;
		}
				
		$len = strlen($str);
		
		while ($len > 0)
		{
			// If end tag then break
			if (substr($str, 0, $end_tag_len) == $this->end_tag)
			{
				$str = substr($str, $end_tag_len);
				break;
			}
			
			$script .= $str[0];
			$str = substr($str, 1);
			$len = strlen($str);
		}
		
		return $str;
	}
	
	/**
	 * Strip line feed characters
	 */
	function strip_lf($str)
	{
		$rep = "";
		$len = strlen($str);
		
		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];
			
			if (ord($chr) != 10)
				$rep .= $chr;
		}
		
		return $rep;
	}
	
	/**
	 * Replace all <br> with \r\n.
	 */
	function replace_br($str)
	{
		$rep = "";
		$len = strlen($str);
		$tag = false;
		$tag_str = "";
		$tag_code = "";
		$is_br = false;
		
		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];
			
			if ($chr == "<")
			{
				$tag_str = $chr;
				$tag_code = "";
				$tag = true;
			}
			else if ($chr == ">")
			{
				$tag_code = strtolower($tag_code);
				
				if ($tag_code == "br")
					$rep .= "\r\n";
				else
					$rep .= $tag_str . $chr;
					
				$tag_str = "";
				$tag_code = "";
				$tag = false;
			}
			else
			{
				if ($tag)
				{
					$tag_str .= $chr;
					
					if (ctype_alpha($chr))
						$tag_code .= $chr;
				}
				else
					$rep .= $chr;
			}
		}
		
		return $rep;
	}
	
	/**
	 * Convert HTML entities into characters
	 */
	function htmlsc_decode($str)
	{
		$str = str_replace("&amp;", "&", $str);
		$str = str_replace("&quot;", "\"", $str);
		$str = str_replace("&#039;", "'", $str);
		$str = str_replace("&lt;", "<", $str);
		$str = str_replace("&gt;", ">", $str);
		return $str;
	}
	
	function get_html_code($str, &$code, &$len)
	{
		if (substr($str, 0, 5) == "&amp;")
		{
			$code = "&";
			$len = 5;
			return true;
		}
		
		if (substr($str, 0, 6) == "&quot;")
		{
			$code = "\"";
			$len = 6;
			return true;
		}
		
		if (substr($str, 0, 6) == "&#039;")
		{
			$code = "'";
			$len = 6;
			return true;
		}
		
		if (substr($str, 0, 4) == "&lt;")
		{
			$code = "<";
			$len = 4;
			return true;
		}
		
		if (substr($str, 0, 4) == "&gt;")
		{
			$code = ">";
			$len = 4;
			return true;
		}
		
		return false;		
	}
	
	/**
	 * Strip all tags like PHP function strip_tag
	 * except <br> which will be converted to "\r\n", 
	 * and except tags inside double-quoted strings.
	 */
	function strip_tags_xstr($str)
	{
		$rst = "";
		$cstr = "";
		$instr = false;
		
		$len = strlen($str);
		
		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];
		
			/*
			if ($chr == "&")
			{
				$s = substr($str, $i);
				
				if (!$instr && get_html_code($s, $code, $clen))
				{
					$cstr .= $code;
					$len += $clen;
					continue;
				}
				
				$cstr .= $chr;			
			}
			else */ 
			if ($chr == "\"")
			{
				if (!$instr)
				{
					$cstr = strip_tags($cstr, "<br>");
					$cstr = $this->replace_br($cstr);
					$cstr = $this->htmlsc_decode($cstr);
					$instr = true;
				}
				else
				{
					$cstr = "\"" . $cstr . "\"";
					$instr = false;
				}
				
				$rst .= $cstr;
				$cstr = "";
			}
			else if ($chr == "\\")
			{
				$cstr .= $chr;

				if ($instr) // string escape character
				{
					++$i;
					
					if ($i >= $len)
						break;
						
					$cstr .= $str[$i];
				}
			}
			else
				$cstr .= $chr;
		}
		
		return $rst . $cstr;
	}
	
	/**
	 * Evaluate text with lisp script (equivalent to embedded PHP script).
	 * Lisp script is enclosed within tag [lisp /] or [lispe /]
	 * Tag lispe will print the result of the last expression
	 * while tag lisp will not. A special external lisp function print()
	 * can be used to print output from the tagged lisp expressions.
	 * @RETURN
	 * All text outside the tags with embedded text from the lisp
	 * expression within the tags.  
	 */
	function eval_text($text, $source)
	{
		$start_tag_len = strlen($this->start_tag);
		$end_tag_len = strlen($this->end_tag);
		
		$this->cur_source = $source;
		$this->output_text = "";
		$xchr = "";
		$len = strlen($text);
		
		while ($len > 0)
		{
			if ($this->is_black_flagged(-1))
			{
				$this->serr(lisp_err_black_flagged, $this->cur_row, 
							$this->cur_column, $source);
				$this->perr();
				return $this->output_text;
			}

			if ($xchr != "'")
			{
				// Extract script from text
				$text = $this->skip_tags($text, $script, $print_rst, $has_tags);
			}
			else
				$has_tags = false;
			
			if ($has_tags)
			{	
				$script = $this->strip_tags_xstr($script);
				$rst = null;
				$len = strlen($script);

				$cur = $this->cur_get();
				$this->cur_start(0, 0);
			
				// Evaluate lisp script
				while ($len > 0)
				{
					if ($this->is_black_flagged(-1))
					{
						$this->serr(lisp_err_black_flagged, $this->cur_row, 
									$this->cur_column, $source);
						$this->perr();
						$this->cur_set($cur);
						return $this->output_text;
					}

					$ele = "";
					$type = eletype_unknown;
					$qcnt = 0;
					$row = 0;
					$col = 0;
					$script = $this->skip_element($script, $ele, $type, $qcnt, 
												  $row, $col);

					if ($this->is_err())
					{
						$this->perr();
						$this->cur_set($cur);
						return $this->output_text;
					}
						
					if (strlen($ele) > 0)
					{ 
						$ele_form = array($type, array($qcnt, $row, $col), $ele);
					
						$this->cur_start($row, $col);
						$expr = $this->form_to_string($ele_form);
						$rst = $this->evaluate($expr, $source, $dre);
		
						if ($this->is_err())
						{
							$this->perr();
							$this->cur_set($cur);
							return $this->output_text;
						}
					}
					
					$len = strlen($script);
				}

				$this->cur_set($cur);

				if ($print_rst)
				{
					if ($rst != null && is_array($rst))
					{
						$rst_str = $this->form_to_string($this->last_ele($rst));

						if (!$this->is_nil($rst_str)) // Don't print nil
							$this->output_text .= $this->strip_dquotes($rst_str);
					}
				}
					
				// Evaluate after eval_text()
				if ($this->after_eval_text_func)
				{	
					$fn = $this->after_eval_text_func;
					$this->after_eval_text_func = null; // reset, one time only
			
					if (strlen($fn) > 0)
					{	
						$expr = "(" . $fn . " \"" . $this->output_text . "\")";
						$cur = $this->cur_get();
						$this->cur_start(0, 0);
						$rst = $this->evaluate($expr, $source . ": " .
									   		   $this-t("After eval_text()"), $dre);

						if ($this->is_err())
						{
							$this->perr();
							$this->cur_set($cur);
							return $this->output_text;
						}

						$this->cur_set($cur);
				
						if ($rst != null && is_array($rst))
						{
							$rst_str = $this->form_to_string($this->last_ele($rst));

							if (!$this->is_nil($rst_str)) // Don't print nil
								$this->output_text = $this->strip_dquotes($rst_str);
							else
								$this->output_text = "";
						}		
					}
				}
			}
			else
			{
				// Non-lisp-script: outside lisp/lispe tags 
				$xchr = $text[0];
				$this->output_text .= $xchr;
				$text = substr($text, 1);
			}
			
			$len = strlen($text);
		}

		return $this->output_text;
	}

	/**
	 * Almost similar to eval_text except that no expression
	 * get evaluated. Instead, all expressions are replaced with
	 * the replacement string $replace_with.
	 * This function is used to strip all expression in the text
	 * so that no code will be exposed when it is published to
	 * the end user. This is important when the end user is 
	 * restricted from accesing the code. 
	 */
	function replace_eval_text($text, $source, $replace_with)
	{
		$this->cur_source = $source;
		$this->output_text = "";
		$xchr = "";
		$len = strlen($text);
		
		while ($len > 0)
		{
			if ($this->is_black_flagged(-1))
			{
				$this->serr(lisp_err_black_flagged, $this->cur_row, 
							$this->cur_column, $source);
				$this->perr();
				return $this->output_text;
			}

			if ($xchr != "'")
			{
				// Extract script from text
				$text = $this->skip_tags($text, $script, $print_rst, $has_tags);
			}
			else
				$has_tags = false;
			
			if ($has_tags)
				$this->output_text .= $replace_with;
			else
			{
				// Non-lisp-script: outside lisp/lispe tags 
				$xchr = $text[0];
				$this->output_text .= $xchr;
				$text = substr($text, 1);
			}
			
			$len = strlen($text);
		}

		return $this->output_text;
	}
}
?>

