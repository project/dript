<?php  
/**
 * Class Lisp Parser
 * By Abdullah Daud
 * 8 January 2007
 * Note: This code was from the lisp class that gets too long.
 * In turn, lisp class extends this class.
 */
class lisp_parser
{
	// Error variables
	var $error_code = 0; // Error code (0 is ok)
	var $error_column = 0;
	var $error_row = 0;
	var $error_source = "";

	var $cur_source = ""; // Current source
	var $cur_column = 0; // Current column
	var $cur_row = 0; // Current row

	/**
	 * Class lisp_parser contructor.
	 * All global constants are declared here.
	 */
	function init_lisp_parser()
	{
		define("lisp_err_ok", 0);
		define("lisp_err_black_flagged", -1);
		define("lisp_err_unexpected_end_of_form", -2);
		define("lisp_err_expecting_end_of_string", -3);
		define("lisp_err_func_general", -4);
		define("lisp_err_func_arg", -5);
		define("lisp_err_divide_by_zero", -6);

		define("lisp_eletype_unknown", 0);
		define("lisp_eletype_raw_form", 1);
		define("lisp_eletype_raw_string", 2);
		define("lisp_eletype_raw_word", 3);
		define("lisp_eletype_form", 4);
		define("lisp_eletype_string", 5);
		define("lisp_eletype_word", 6);

	}

	function debug($title, $msg)
	{
		echo "<br>DEBUG " . $title . " - " . $msg . "<br>";
	}
	
	function debug_r($title, $array_msg)
	{
		echo "<br>DEBUG " . $title . "<pre>";
		print_r($array_msg);
		echo "</pre>";
	}
	
	/**
	 * Get error string
	 */
	function gerr_str($code)
	{
		switch ($code)
		{
			case lisp_err_ok: 
				return "ok";
			case lisp_err_black_flagged: 
				return "black flagged";
			case lisp_err_unexpected_end_of_form:
				return "unexpected end of form";
			case lisp_err_expecting_end_of_string:
				return "expecting end of string";
			case lisp_err_func_general:
				return "function general";
			case lisp_err_func_arg:
				return "function argument";
			case lisp_err_divide_by_zero:
				return "divide by zero"; 
		}

		return "unknown";		
	}
	
	/**
	 * Set error code
	 */
	function serr($code, $row, $col, $source)
	{
		$this->error_code = $code;
		$this->error_row = $row;
		$this->error_column = $col;
		$this->error_source = $source;
	}

	/**
	 * Set error code only
	 */
	function serr_code($code)
	{
		$this->error_code = $code;
		$this->error_row = -1;
		$this->error_column = -1;
		$this->error_source = "";
	}

	/**
	 * Set error code using current row, column and source
	 */
	function serr_cur($code)
	{
		$this->error_code = $code;
		$this->error_row = $cur_row;
		$this->error_column = $cur_col;
		$this->error_source = $cur_source;
	}

	/**
	 * Clear error code
	 */
	function cerr()
	{
		$this->error_code = lisp_err_ok;
		$this->error_row = -1;
		$this->error_column = -1;
		$this->error_source = "";
	}

	/**
	 * Return true on error
	 */
	function is_err()
	{
		if ($this->error_code != lisp_err_ok)
			return true;

		return false;
	}

	/*
	 * Set the cursor's column and row from the array
	 */
	function cur_set($cur)
	{
		$this->cur_column = $cur[0];
		$this->cur_row = $cur[1];
	}

	/**
	 * Return the cursor's column and row in an array
	 */
	function cur_get()
	{
		return array($this->cur_column, $this->cur_row);
	}

	/**
	 * Start cursor at the specified row ($r) and column ($c)
	 */
	function cur_start($r, $c)
	{
		$this->cur_column = $c;
		$this->cur_row = $r;
	}

	/**
	 * Increment cursor to the next column
	 */
	function cur_inc($step = 1)
	{
		for ($i = 0; $i < $step; $i++)
			++$this->cur_column;
	}

	/*
	 * Move cursor to the beginning of the next line
	 */
	function cur_nl()
	{
		++$this->cur_row;
		$this->cur_column = 0;
	}

	/**
	 * Check if chr is a valid lisp space
	 */
	function is_space($chr)
	{
		if (ctype_space($chr))
			return true;
			
		if (ctype_cntrl($chr))
			return true;
			  
		if (!ctype_graph($chr))
			return true;
			
		if (ord($chr) <= 32)
			return true;
			
		return false;
	}
	
	/**
	 * Check if chr is a lisp control character
	 */
	function is_ctrl_chr($chr)
	{
		if ($chr == "(" ||
			$chr == ")" ||
			$chr == "'" ||
			$chr == ";" ||
			$chr == "\"")
			return true;
			
		return false;
	}

	/**
	 * Check if $str is a valid word.
	 * A valid word must not contain white spaces and
	 * lisp special characters: {(}, {)}, {'}, {;} and {"}.
	 */
	function is_word($str)
	{
		$str = trim($str);
		
		$len = strlen($str);
		
		if ($len <= 0) 
			return false;
			
		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];
			
			if (ctype_space($chr) || $this->is_ctrl_chr($chr))
				return false;
		}
		
		return true;
	}
	
	/**
	 * Check if $str is a valid word and name
	 */
	function is_name($str)
	{
		$str = trim($str);
		
		if (!$this->is_word($str))
			return false;
			
		$len = strlen($str);
		
		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];
			
			if (ctype_alpha($chr))
				continue;
				
			if ($chr != "-" &&
				$chr != "_" &&
				$chr != "." &&
				$chr != "?" &&
				$chr != "|" &&
				$chr != ":" &&
				$chr != "+" &&
				$chr != "=" &&
				$chr != "*" &&
				$chr != "&" &&
				$chr != "^" &&
				$chr != "%" &&
				$chr != "$" &&
				$chr != "#" &&
				$chr != "@" &&
				$chr != "!" &&
				$chr != "~")
				return false; 
		}	
		
		return true;
	}
	
	/**
	 * Check if $str is a valid integer
	 */
	function is_int($str)
	{
		$str = trim($str);
		
		if (!$this->is_word($str))
			return false;
			
		$len = strlen($str);
		
		if ($len <= 0)
			return false;
			
		$chr = $str[0];		
	
		if ($chr == "+" || $chr == "-")
			$i = 1;
		else
			$i = 0;
			
		for (; $i < $len; $i++)
		{
			$chr = $str[$i];		
			
			if (!ctype_digit($chr))
				return false;
		}
					
		return true;
	}
	
	/**
	 * Check if $str is a valid number or integer
	 */
	function is_num($str)
	{
		$str = trim($str);
		
		if (!$this->is_word($str))
			return false;
			
		if ($this->is_int($str))
			return true;
			
		$len = strlen($str);
		
		if ($len <= 0)
			return false;
			
		$chr = $str[0];		
	
		if ($chr == "+" || $chr == "-")
		{
			$xchr = $chr;
			$i = 1;
		}
		else
		{
			$xchr = "";
			$i = 0;
		}
		
		$p = false;	
		$e = false;
		
		for (; $i < $len; $i++)
		{
			$chr = $str[$i];		
			
			if ($chr == ".")
			{
				if ($p) // cannot have multiple points
					return false;
				
				if ($e) // cannot have point after exponential mark
					return false;
						
				if (!ctype_digit($xchr)) // point must be preceeded with a digit
					return false;
				
				$p = true;
			}
			else if ($chr == "e" || $chr == "E")
			{
				if ($e) // cannot have multiple exponential marks
					return false;

				if ($xchr == ".") // point cannot preceed exponential mark
					return false;
										
				$e = true;
			}
			else if ($chr == "+" || $chr == "-")
			{
				// sign is allowed only immediately after the exponential mark
				if ($xchr != "e" && $xchr != "E")
					return false;  
			}
			else if (!ctype_digit($chr))
				return false;
			
			$xchr = $chr;
		}
					
		return true;	
	}
	
	/**
	 * Convert string to value when it is a number
	 */
	function get_value($str)
	{
		$str = trim($str);
		
		if (!$this->is_num($str))
			return;
			
		sscanf($str, "%e", $v);
		return $v;
	}
	
	/**
	 * Return str with the beginning white spaces removed
	 */
	function skip_spaces($str)
	{
		$len = strlen($str);

		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];

			if ($chr == "\r")
			{
				$this->cur_nl();

				if ($str[$i+1] == "\n")
					++$i;

				continue;
			}
			else if ($chr == "\n")
			{
				$this->cur_nl();

				if ($str[$i+1] == "\r")
					++$i;

				continue;
			}
			else if (!$this->is_space($chr))
				return substr($str, $i);

			$this->cur_inc();
		}

		return "";
	}

	/*
	 * Return str with the line comment removed
	 * and set the comment in variable $comment with ';' and line break
	 * removed.
	 */
	function skip_comment($str, &$comment)
	{
		$comment = "";
		$len = strlen($str);

		if ($len <= 0)
			return "";

		if ($str[0] != ";")
			return $str;

		for ($i = 1; $i < $len; $i++)
		{
			$chr = $str[$i];

			if ($chr == "\r")
			{
				$this->cur_nl();
				$i1 = $i + 1;
					
				if ($i1 >= $len)
					return "";
					
				if ($str[$i1] == "\n")
				{
					$i2 = $i + 2;
					
					if ($i2 >= $len)
						return "";
						
 					return substr($str, $i2);
				}
				else
					return substr($str, $i1);
			}
			else if ($chr == "\n")
			{
				$this->cur_nl();
				$i1 = $i + 1;
					
				if ($i1 >= $len)
					return "";
					
				if ($str[$i] == "\r")
				{
					$i2 = $i + 2;
					
					if ($i2 >= $len)
						return "";
						
					return substr($str, $i2);
				}
				else
					return substr($str, $i1);
			}
			else
				$comment .= $chr;

			$this->cur_inc();
		}

		return "";
	}
	
	/**
	 * Return str with the first word removed and
	 * set the word in the $word variable.
	 */
	function skip_word($str, &$word)
	{
		$word = "";
		$len = strlen($str);

		if ($len <= 0)
			return "";

		if (!ctype_graph($str[0]))
			return $str;

		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];

			if ($this->is_space($chr) || $this->is_ctrl_chr($chr))
				return substr($str, $i);
			else
				$word .= $chr;

			$this->cur_inc();
		}

		return "";
	}

	/**
	 * Return str with the first string removed and
	 * set the string in the $string variable with the quote removed.
	 * A string cannot span more than one line.
	 * Once a line feed is detected the string will be truncated.
	 */
	function skip_string($str, &$string, &$truncated)
	{
		$string = "";
		$truncated = false;
		$len = strlen($str);

		if ($len <= 0)
			return "";

		if ($str[0] != "\"")
			return $str;

		$this->cur_inc();

		for ($i = 1; $i < $len; $i++)
		{
			$chr = $str[$i];

			if ($chr == "\"" )
			{
				$this->cur_inc();
				return substr($str, $i + 1);
			}
			else if ($chr == "\\")
			{
				$string .= $chr;
				$this->cur_inc();
				++$i;
				
				if ($i >= $len)
					break;
					
				$chr = $str[$i];
				$string .= $chr;
			}
			else if ($chr == "\r")
			{
				$this->cur_nl();
				$truncated = true;
				$i1 = $i + 1;

				if ($i1 >= $len)
					return "";
					
				if ($str[$i1] == "\n")
				{
					$i2 = $i + 2;

					if ($i2 >= $len)
						return "";
					
					return substr($str, $i2);
				}
				else
					return substr($str, $i1);
			}
			else if ($chr == "\n")
			{
				$this->cur_nl();
				$truncated = true;
				$i1 = $i + 1;

				if ($i1 >= $len)
					return "";
					
				if ($str[$i1] == "\r")
				{
					$i2 = $i + 2;

					if ($i2 >= $len)
						return "";
					
					return substr($str, $i2);
				}
				else
					return substr($str, $i1);
			}
			else
				$string .= $chr;

			$this->cur_inc();
		}

		return "";
	}

	/**
	 * Return str with the first form removed and
	 * set the form in the $form variable with the parenteses removed.
	 */
	function skip_form($str, &$form)
	{
		$form = "";
		$len = strlen($str);

		if ($len <= 0)
			return "";

		if ($str[0] != "(")
			return $str;

		$this->cur_inc();

		$ifcnt = 0;

		for ($i = 1; $i < $len; $i++)
		{
			$chr = $str[$i];

			if ($chr == ")")
			{
				if ($ifcnt > 0)
				{
					--$ifcnt;
					$form .= $chr;
				}
				else
				{
					$this->cur_inc();
					$i1 = $i + 1;
					
					if ($i1 >= $len)
						return "";
						
					return substr($str, $i1);
				}
			}
			else if ($chr == "(")
			{
				++$ifcnt;
				$form .= $chr;
			}
			else if ($chr == ";") // start of comment
			{
				$comment = "";
				$str = $this->skip_comment(substr($str, $i), $comment);
				$form .= ";" . $comment . "\r\n";
				$len = strlen($str);
				$i = -1;
			}
			else if ($chr == "\"") // start of string
			{
				$string = "";
				$trun = false;
				$str = $this->skip_string(substr($str, $i), $string, $trun);

				if ($trun)
				{
					$this->serr_cur(lisp_err_expecting_end_of_string);
					return;
				}

				$form .= "\"" . $string . "\"";
				$len = strlen($str);
				$i = -1;
			}
			else if ($chr == "\n")
			{
				$this->cur_nl();
				$form .= $chr;
				$i1 = $i + 1;
				
				if ($i1 >= $len)
					break;

				$chr1 = $str[$i1];

				if ($chr1 == "\r")
				{
					$form .= $chr1;
					++$i;
				}

				continue;
			}
			else if ($chr == "\r")
			{
				$this->cur_nl();
				$form .= $chr;
				$i1 = $i + 1;
				
				if ($i1 >= $len)
					break;

				$chr1 = $str[$i1];

				if ($chr1 == "\n")
				{
					$form .= $chr1;
					++$i;
				}

				continue;
			}
			else
				$form .= $chr;

			$this->cur_inc();
		}

		return "";
	}

	/**
	 * Return str with quotes removed
	 * and set the number of quotes in variable $count
	 */
	function skip_quotes($str, &$count)
	{
		$count = 0;
		$len = strlen($str);

		for ($i = 0; $i < $len; $i++)
		{
			$chr = $str[$i];
			
			if ($chr == "'")
				++$count;
			else
				return substr($str, $i);

			$this->cur_inc();
		}

		return "";
	}

	/**
	 * Return str with the first element removed
	 * and set the element in variable $element
	 * and set the element type in variable $type
	 * and set the element quote count in variable $quote_count
	 */
	function skip_element($str, &$element, &$type, &$quote_count, &$row, &$col)
	{
		$element = "";
		$type = lisp_eletype_unknown;
		$quote_count = 0;
		$row = -1;
		$col = -1;	
		$str = $this->skip_spaces($str);
		$len = strlen($str);

		// Skip comment blocks and spaces
		while ($len > 0)
		{
			if ($str[0] != ";")
				break;

			$str = $this->skip_comment($str, $cmt);
			$str = $this->skip_spaces($str);
			$len = strlen($str);
		}

		if ($len <= 0)
			return "";

		$str = $this->skip_quotes($str, $quote_count);
		$len = strlen($str);

		if ($len <= 0)
			return "";

		$row = $this->cur_row;
		$col = $this->cur_column;
		$chr = $str[0];

		if ($chr == "(")
		{
			$type = lisp_eletype_form;
			return $this->skip_form($str, $element);
		}

		if ($chr == "\"")
		{
			$trun = false;
			$ret = $this->skip_string($str, $element, $trun);

			if ($trun)
			{
				$this->serr_cur(lisp_err_expecting_end_of_string);
				return "";
			}

			$type = lisp_eletype_string;
			return $ret;
		}

		if ($chr == ")")
		{
			$this->serr_cur(lisp_err_unexpected_end_of_form);
			return "";
		}

		$type = lisp_eletype_word;

		return $this->skip_word($str, $element);
	}

	/**
	 * Similar to skip_element() except that it returns form
	 */
	function skip_element_form($str, &$form)
	{
		$str = $this->skip_element($str, $ele, $type, $qcnt, $row, $col);
		$form = array($type, array($qcnt, $row, $col), $ele);	
		
		return $str;
	}
			
}
?>