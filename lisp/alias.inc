<?php
/**
 *	Get function aliases for the lisp interpreter
 *	By Abdullah Daud
 *	21 December 2006
 *
 *	Since each function reside in a file of the same name
 *	that can cause file naming problem. Add function name
 *	alias here whenever a function name may cause confict
 *	with the operating system file naming. The function
 *	name is at the left and the file name is at the right.
 *
 *	The alias can also be used to create lisp dialects for
 *  various human languages. But care must be taken since
 *	scripts with different alias tables will not be
 *	compatible to each other. It is right to say that lisp
 *	scripts with different alias tables are totally in
 *	different languages.
 */

function lisp_get_function_aliases()
{
	return array('@' => 'comment',
				 '-' => 'substract',
				 '+' => 'add',
				 '*' => 'multiply',
				 '/' => 'divide',
				 '=' => 'eq',
				 '!=' => 'neq',
				 '>' => 'gt',
				 '<' => 'lt',
				 '>=' => 'gte',
				 '<=' => 'lte',
				 '+1' => 'inc',
				 '-1' => 'dec');
}

?>