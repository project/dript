<?php
/**
 * Lisp function: syntax
 * By Abduulah Daud
 * 27 December 2006
 */
function create_lisp_func_syntax()
{
	return new lisp_func_syntax();
}

class lisp_func_syntax
{
	function name(&$core)
	{
		return $core->get_func_alias('syntax');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'func-name) => the function syntax',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the syntax of a function');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$func_type = $expr[3][0]; // arg 1
		$func_name = $expr[3][2];

		if ($func_type == lisp_eletype_word && strlen($func_name) > 0)
		{
			$func = $core->get_func($func_name);
			$syn = $core->func_special_attr($func, 'syntax');
			return array(lisp_eletype_string, array(0, -1, -1), $syn);
		}
	}
}
?>
