<?php
// Lisp function: multiply
// By Abdullah Daud
// 20 December 2006

function create_lisp_func_multiply()
{
	return new lisp_func_multiply();
}

class lisp_func_multiply
{
	function name(&$core)
	{
		return $core->get_func_alias('multiply');
	}

	function syntax(&$core)
	{
		return $core->t('%name value ...) => The result of multiplying all the values',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Multiply all the values and return the result');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = 0.0;
		$arg_val = $core->get_value($expr[3][2]);

		if (!is_null($arg_val))
			$val = $arg_val;

		for ($i = 4; $i < $cnt; $i++)
		{
			$arg_val = $core->get_value($expr[$i][2]);

			if (!is_null($arg_val))
				$val *= $arg_val;
		}

		return array(lisp_eletype_word, array(0, -1, -1), $val);
	}
}
?>