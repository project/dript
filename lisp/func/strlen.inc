<?php
// Lisp function: strlen
// By Abdullah Daud
// 28 December 2006

function create_lisp_func_strlen()
{
	return new lisp_func_strlen();
}

class lisp_func_strlen
{
	function name(&$core)
	{
		return $core->get_func_alias('strlen');
	}

	function syntax(&$core)
	{
		return $core->t('(%name string ...) => lengths of strings',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('String length. Return the string length or a list of the string lengths.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		if ($cnt == 4)
		{
			$len = strlen($expr[3][2]);
			return array(lisp_eletype_word, array(0, -1, -1), $len);
		}

		$form = array(lisp_eletype_form, array(0, -1, -1));
				
		for ($i = 3; $i < $cnt; $i++)
		{
			$s = $core->form_to_string($expr[$i]);
			
			if (!$core->is_nil($s)) // Don't include nil
				$len = strlen($core->strip_dquotes($s));
			else
				$len = 0;

			$form[] = array(lisp_eletype_word, array(0, -1, -1), $len);
		}

		return $form;
	}
}
?>