<?php
// Lisp function: drupal-user-roles
// By Abdullah Daud
// 6 January 2007

function create_lisp_func_drupal_user_roles()
{
	return new lisp_func_drupal_user_roles();
}

class lisp_func_drupal_user_roles
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-user-roles');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => The roles of the current user.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the roles of the current user. ' .
						'Return the list of roles.'); 
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$user = $GLOBALS['user'];
		
		if (!is_object($user))
			return;
			
		$user_arr = get_object_vars($user);
		
		if (!is_array($user_arr))
			return;
			
		$user_roles = $user_arr['roles'];
		
		if (!is_array($user_roles))
			return;
			
		$form = array(lisp_eletype_form, array(0, -1, -1));
		
		foreach ($user_roles as $key => $val)
		{
			if (strlen($val) > 0)
				$form[] = array(lisp_eletype_string, array(0, -1, -1), $val);
		}
		
		return $form;
	}
}
?>