<?php
// Lisp function: cons
// By Abdullah Daud
// 29 December 2006

function create_lisp_func_cons()
{
	return new lisp_func_cons();
}

class lisp_func_cons
{
	function name(&$core)
	{
		return $core->get_func_alias('cons');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'ele_x \'(ele_1 ...)) => (ele_x ele_1 ...)',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Push the first argument element into the second ' . 
						'argument list. Return the expanded list.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$arg1 = $expr[3];
		$arg1_type = $arg1[0];
		$arg1_attr = $arg1[1];
		$arg1_val = $arg1[2];

		// Prepare the first element		
		if ($arg1_type == lisp_eletype_form)
		{
			$first_ele = array($arg1_type, array($arg1_attr[0], -1, -1));
			$cnt2 = count($arg1);
		
			for ($i = 2; $i < $cnt2; $i++)
			{
				$ele = $arg1[$i];
					
				if (is_array($ele))
					$first_ele[] = array($ele[0], array($ele[1][0], -1, -1), $ele[2]);
				else
					$first_ele[] = $ele;
			}
		}
		else
			$first_ele = array($arg1_type, array($arg1_attr[0], -1, -1), $arg1_val);
		
		if ($cnt == 4 || $core->is_nil($expr[4])) // only one argument specified
		{
			$form = array(lisp_eletype_form, array(0, -1, -1));
			$form[] = $first_ele;
			return $form;
		}
		
		$arg2 = $expr[4];
		$arg2_type = $arg2[0];
		$arg2_attr = $arg2[1];
		$arg2_val = $arg2[2];
		
		if ($arg2_type == lisp_eletype_form)
		{
			if ($cnt > 3)
			{
				$form = array(lisp_eletype_form, array($arg2_attr[0], -1, -1));
				$form[] = $first_ele;
				
				$cnt = count($arg2);
			
				for ($i = 2; $i < $cnt; $i++)
				{
					$ele = $arg2[$i];
					$form[] = array($ele[0], array($ele[1][0], $ele[1][1], $ele[1][2]), $ele[2]);
				}
			
				return $form;
			}
		}
		else if ($arg2_type == lisp_eletype_raw_form)
		{
			$str = substr($arg2_val, 1); // skip '('
			
			$cur = $core->cur_get();
			$core->cur_start($arg2_attr[1],$arg2_attr[2]);
			$str = $core->skip_spaces($str);
			$core->cur_set($cur);
			
			if (strlen($str) > 1)
				$rst = '(' . $core->form_to_string($first_ele) . ' ' . $str;
			else
				$rst = '(' . $core->form_to_string($first_ele) . ')';
			
			return array($arg2_type, array($arg2_attr[0], -1, -1), $rst); 
		}
	}
}
?>