<?php
/**
 * Lisp function: foreach
 * By Abduulah Daud
 * 30 December 2006
 */
function create_lisp_func_foreach()
{
	return new lisp_func_foreach();
}

class lisp_func_foreach
{
	function name(&$core)
	{
		return $core->get_func_alias('foreach');
	}

	function syntax(&$core)
	{
		return $core->t('(%name var (ele ...) expr ...) ' . 
						'=> the result of the last expression',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Assign to the variable each element in the list ' . 
						'and for every assignment evaluate all expressions. ' . 
						'Return the result of the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_array($expr))
			return;
			
		$cnt = count($expr);
		
		if ($cnt < 5)
			return; // Not enough arguments
			
		$var = $expr[3][2];
		
		if (!$core->is_word($var))
			return; // Not a valid variable name
			
		$list = $expr[4][2];
		
		if ($list[0] != '(')
			return; // Not a list
					
		$list = substr($list, 1);
		$last_rst = null;
		$cur = $core->cur_get();
		$core->cur_set($expr[4][1][1], $expr[4][1][2] + 1); // list's cursor 
		$list_cur = $core->cur_get();

		// Assign each element to the variable
		while (strlen($list) > 1 && $list != ')')
		{		
			// Read an element from the list
			$core->cur_set($list_cur);
			$list = $core->skip_element($list, $ele, $type, $qcnt, $row, $col);
			$list = $core->skip_spaces($list);
			$list_cur = $core->cur_get();
			
			if ($core->is_err())
			{
				$core->cur_set($cur);
				return;
			}
			
			// Assign the element to the variable
			$core->set_local_var($var, $type, $qcnt, $ele);			
			
			// Evaluate all expression
			for ($i = 5; $i < $cnt; $i++)
			{
				$c_expr = $expr[$i];
				$c_attr = $expr[$i][1]; 
				$core->cur_start($c_attr[1], $c_attr[2]);
				$last_rst = $core->evaluate($core->form_to_string($c_expr), $source, $dre);
		
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}
			}
		}

		$core->cur_set($cur);

		if ($last_rst != null && is_array($last_rst))
		{
			if ($last_rst[0] == lisp_eletype_form)
				return $last_rst[2];
		}
	}
}
?>