<?php
// Lisp function: add
// By Abdullah Daud
// 20 December 2006

function create_lisp_func_add()
{
	return new lisp_func_add();
}

class lisp_func_add
{
	function name(&$core)
	{
		return $core->get_func_alias('add');
	}

	function syntax(&$core)
	{
		return $core->t('(%name value ...) => sum of values',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Add all values and return the result');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = 0.0;

		for ($i = 3; $i < $cnt; $i++)
		{
			$arg_val = $core->get_value($expr[$i][2]);

			if (!is_null($arg_val))
				$val += $arg_val;
		}

		return array(lisp_eletype_word, array(0, -1, -1), $val);
	}
}
?>