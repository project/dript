<?php
// Lisp function: is-num
// By Abdullah Daud
// 9 January 2007

function create_lisp_func_is_num()
{
	return new lisp_func_is_num();
}

class lisp_func_is_num
{
	function name(&$core)
	{
		return $core->get_func_alias('is-num');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'ele ...) => T when all elements are numbers',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Test element for number including number with ' . 
						'exponential notation. Return T when all elements ' . 
						'are numbers otherwise return nil.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		for ($i = 3; $i < $cnt; $i++)
		{
			if (!$core->is_num($expr[$i][2]))
				return;
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>