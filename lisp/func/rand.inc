<?php
// Lisp function: rand
// By Abdullah Daud
// 10 January 2007

function create_lisp_func_rand()
{
	return new lisp_func_rand();
}

class lisp_func_rand
{
	function name(&$core)
	{
		return $core->get_func_alias('rand');
	}

	function syntax(&$core)
	{
		return $core->t('(%name {min max}) => A random number.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Generate an integer random number. If a minimum and ' .
						'a maximum are given then a random number from ' .
						'the minimum to the maximum will be taken. ' . 
						'Return the generated random number.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt >= 5)
		{
			$min = $core->get_value($expr[3][2]);
			$max = $core->get_value($expr[4][2]);
			$r = rand((int)$min, (int)$max);			
		}
		else
			$r = rand();

		return array(lisp_eletype_word, array(0, -1, -1), $r);
	}
}
?>