<?php
/**
 * Lisp function: cond
 * By Abduulah Daud
 * 31 December 2006
 */
function create_lisp_func_cond()
{
	return new lisp_func_cond();
}

class lisp_func_cond
{
	function name(&$core)
	{
		return $core->get_func_alias('cond');
	}

	function syntax(&$core)
	{
		return $core->t('(%name (ctrl-expr sub-expr ... ) ... ) ' . 
						'=> the result of the last expression',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Evaluate ctrl-expr in every list until a non-nil result ' . 
						'is achieved. When the ctrl-expr is non-nil then ' .  
						'evaluate the remaining sub-expr and ' . 
						'return the result of the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function _cond(&$core, &$expr, &$source, &$rst)
	{
		if (!is_array($expr))
			return false;
			
		$expr_str = $expr[2];
		
		if ($expr_str[0] != '(')
			return false;
			
		// Retrieve and evaluate the control expression
		
		$expr_str = substr($expr_str, 1); // skip '('
		$ele = '';
		$type = eletype_unknown;
		$qcnt = 0;
		$row = 0;
		$col = 0;
			
		$cur = $core->cur_get();

		// Retrieve the expression from string
		$core->cur_start($expr[1][1], $expr[1][2] + 1);
		$expr_str = $core->skip_element($expr_str, $ele, $type, $qcnt, $row, $col);
		$expr_str = $core->skip_spaces($expr_str);	
		$expr_cur = $core->cur_get();
		// Evaluate the expression
		$form = array($type, array($qcnt, $row, $col), $ele);	
		$core->cur_start($row, $col);		
		$rst = $core->evaluate($core->form_to_string($form), $source, $dre);			
		
		if ($core->is_err())
		{
			$core->cur_set($cur);
			return false;
		}
			
		if ($core->is_nil($rst[2]))
		{
			$core->cur_set($cur);
			return false;
		}
			
		// Evaluate the sub-expressions
		
		while (strlen($expr_str) > 1 && $expr_str[0] != ')')
		{
			$ele = '';
			$type = eletype_unknown;
			$qcnt = 0;
			$row = 0;
			$col = 0;
			
			// Retrieve the expression from string
			$core->cur_set($expr_cur);
			$expr_str = $core->skip_element($expr_str, $ele, $type, $qcnt, $row, $col);
			$expr_str = $core->skip_spaces($expr_str);	
			$expr_cur = $core->cur_get();
			// Evaluate the expression
			$form = array($type, array($qcnt, $row, $col), $ele);	
			$core->cur_start($row, $col);		
			$rst = $core->evaluate($core->form_to_string($form), $source, $dre);			
		
			if ($core->is_err())
			{
				$core->cur_set($cur);
				return false;
			}
		}
			
		$core->cur_set($cur);
		
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_array($expr))
			return;
			
		$cnt = count($expr);

		for ($i = 3; $i < $cnt; $i++)
		{
			if ($this->_cond($core, $expr[$i], $source, $rst))
				return $core->last_ele($rst[2]);			

			if ($core->is_err())
				return;
		}
	}
}
?>