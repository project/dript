<?php
/**
 * Lisp function: while
 * By Abduulah Daud
 * 28 December 2006
 */
function create_lisp_func_while()
{
	return new lisp_func_while();
}

class lisp_func_while
{
	function name(&$core)
	{
		return $core->get_func_alias('while');
	}

	function syntax(&$core)
	{
		return $core->t('(%name (ctrl-expr) expr ...) => the result of the last expression',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Evaluate the ctrl-expr, if the result is non-nil then evaluate the rest of the expr and repeat. Return the result of the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_array($expr))
			return;
			
		$cnt = count($expr);
		
		if ($cnt < 4)
			return;
			
		if (!is_array($expr[3]))
			return;

		$last_rst = null;
		$cur = $core->cur_get();
		
		while (true)
		{			
			// Evaluate control expression
		
			$ctrl = $expr[3]; // arg 1
			$ctrl_type = $expr[3][0]; 
			$ctrl_attr = $expr[3][1]; 
			$ctrl_expr = $expr[3][2];

			if ($ctrl_attr[0] > 0) // quoted
			{
				$core->cur_set($cur);
				return array($ctrl_type, 
							 array($ctrl_attr[0] - 1, $ctrl_attr[1], $ctrl_attr[2]), 
							 $ctrl_expr);			
			}
			
			$core->cur_start($ctrl_attr[1], $ctrl_attr[2]);
			$rst = $core->evaluate($core->form_to_string($ctrl), $source, $dre);
		
			if ($core->is_err())
			{
				$core->cur_set($cur);
				return;
			}

			// Evaluate the rest of the expr
			
			if ($core->is_nil($rst[2]))
				break;
				
			for ($i = 4; $i < $cnt; $i++)
			{
				$c_expr = $expr[$i];
				$c_attr = $expr[$i][1]; 
				$core->cur_start($c_attr[1], $c_attr[2]);
				$last_rst = $core->evaluate($core->form_to_string($c_expr), $source, $dre);
		
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}
			}
		}

		$core->cur_set($cur);

		if ($last_rst != null && is_array($last_rst))
		{
			if ($last_rst[0] == lisp_eletype_form)
				return $last_rst[2];
		}
	}
}
?>