<?php
// Lisp function: funs
// By Abdullah Daud
// 7 January 2007

function create_lisp_func_funs()
{
	return new lisp_func_funs();
}

class lisp_func_funs
{
	function name(&$core)
	{
		return $core->get_func_alias('funs');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => A list of all function names.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get all function names into a list.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$form = array(lisp_eletype_form, array(0, -1, -1));
		$list = $core->get_all_func();
		
		if (is_array($list))
		{		
			foreach ($list as $key => $val)
			{
				$val = $core->get_func_alias($val);
				
				if ($core->is_word($val))
					$form[] = array(lisp_eletype_word, array(1, -1, -1), $val);
			}
		}
		
		return $form;
	}
}
?>