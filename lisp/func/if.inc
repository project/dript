<?php
/**
 * Lisp function: if
 * By Abduulah Daud
 * 27 December 2006
 */
function create_lisp_func_if()
{
	return new lisp_func_if();
}

class lisp_func_if
{
	function name(&$core)
	{
		return $core->get_func_alias('if');
	}

	function syntax(&$core)
	{
		return $core->t('(%name (ctrl-expr)(non-nil-expr){(nil-expr)}) ' . 
						'=> the result of the last expression',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Evaluate the ctrl-expr, if the result is non-nil ' . 
						'then evaluate the non-nil-expr else evaluate the ' . 
						'nil-expr. Return the result of the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_array($expr))
			return;
			
		if (!is_array($expr[3]))
			return;
			
		// Evaluate control expression
		
		$ctrl = $expr[3]; // arg 1
		$ctrl_type = $expr[3][0]; 
		$ctrl_attr = $expr[3][1]; 
		$ctrl_expr = $expr[3][2];

		if ($ctrl_attr[0] > 0) // quoted
		{
			return array($ctrl_type, 
						 array($ctrl_attr[0] - 1, $ctrl_attr[1], $ctrl_attr[2]), 
						 $ctrl_expr);			
		}
		
		$cur = $core->cur_get();
		$core->cur_start($ctrl_attr[1], $ctrl_attr[2]);
		$rst = $core->evaluate($core->form_to_string($ctrl), $source, $dre);
		
		if ($core->is_err())
		{
			$core->cur_set($cur);
			return;
		}
		
		// Select an expression
		
		if (!$core->is_nil($rst[2]))
			$selx = $expr[4]; // arg 2: non-nil expression
		else
			$selx = $expr[5]; // arg 3: nil expression

		// Evaluate the selected expression
		
		if ($selx == null)
			return;
			 			
		if (!is_array($selx))
			return;	
		
		$selx_type = $selx[0];    
		$selx_attr = $selx[1]; 
		$selx_expr = $selx[2];
				
		if ($selx_attr[0] > 0) // quoted
		{
			return array($selx_type, 
						 array($selx_attr[0] - 1, $selx_attr[1], $selx_attr[2]), 
						 $selx_expr);			
		}
		
		$core->cur_start($selx_attr[1], $selx_attr[2]);
		$rst = $core->evaluate($core->form_to_string($selx), $source, $dre);
		$core->cur_set($cur);
		
		if ($core->is_err())
			return;
			
		return $core->last_ele($rst[2]);			
	}
}
?>