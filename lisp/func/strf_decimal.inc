<?php
// Lisp function: strf-decimal
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_strf_decimal()
{
	return new lisp_func_strf_decimal();
}

class lisp_func_strf_decimal
{
	function name(&$core)
	{
		return $core->get_func_alias('strf-decimal');
	}

	function syntax(&$core)
	{
		return $core->t('(%name value {width=0 {precision=0 {zero-padding=nil}}}) ' . 
						'=> value as formatted decimal string.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Format a value as decimal string.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = $core->get_value($expr[3][2]);

		if (is_null($val))
			return;
			
		$wid = 0;
		$pre = 0;
		$zpad = false;
		
		if ($cnt > 4)
		{
			$a = $core->get_value($expr[4][2]);
			
			if (!is_null($a))
				$wid = (int)$a;
				
			if ($cnt > 5)
			{
				$a = $core->get_value($expr[5][2]);
			
				if (!is_null($a))
					$pre = (int)$a;

				if ($cnt > 6)
				{
					if (!$core->is_nil($expr[6]))
						$zpad = true;
				}		
			}		
		}	
			
		$fmt = "%";
		
		if ($zpad)
			$fmt .= "0";
			
		$fmt .= $wid;
		
		if ($pre > 0)
			$fmt .= "." . $pre;
			
		$fmt .= "f";	
		$str = sprintf($fmt, $val);
		return array(lisp_eletype_string, array(0, -1, -1), $str);
	}
}
?>