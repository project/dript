<?php
// Lisp function: drupal-get-title
// By Abdullah Daud
// 6 January 2007

function create_lisp_func_drupal_get_title()
{
	return new lisp_func_drupal_get_title();
}

class lisp_func_drupal_get_title
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-get-title');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => The title of the page',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the title of the page.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_callable('drupal_get_title')) // external function check
				return;
			
		$title = drupal_get_title();				
		return array(lisp_eletype_string, array(0, -1, -1), $title);
	}
}
?>