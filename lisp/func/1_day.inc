<?php
// Lisp function: 1-day
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_1_day()
{
	return new lisp_func_1_day();
}

class lisp_func_1_day
{
	function name(&$core)
	{
		return $core->get_func_alias('1-day');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => The number of seconds in a day.' . 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the number of seconds in a day.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		return array(lisp_eletype_word, array(0, -1, -1), 86400);
	}
}
?>