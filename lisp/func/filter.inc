<?php
// Lisp function: filter
// By Abdullah Daud
// 25 January 2007

function create_lisp_func_filter()
{
	return new lisp_func_filter();
}

class lisp_func_filter
{
	function name(&$core)
	{
		return $core->get_func_alias('filter');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'having \'(string ...)) => List of strings ' . 
						'matched with the argument \'having\'',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Search the sublist for strings that contain the ' .
						'argument \'having\'. Return a list of matched strings.'); 
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 5)
			return;

		$having = $expr[3][2];

		$arg2 = $expr[4];
		$arg2_type = $arg2[0];
		$arg2_attr = $arg2[1];
		$arg2_val = $arg2[2];
		
		if ($arg2_type == lisp_eletype_form)
		{
			$form = array(lisp_eletype_form, array($arg2_attr[0], -1, -1));
			$cnt = count($arg2);
			
			for ($i = 2; $i < $cnt; $i++)
			{
				$ele = $arg2[$i];
				$pos = strpos($ele[2], $having);
				 
				if ($pos !== false)
					$form[] = array($ele[0], 
									array($ele[1][0], 
										  $ele[1][1], 
										  $ele[1][2]), 
									$ele[2]);
			}
			
			return $form;
		}
		else if ($arg2_type == lisp_eletype_raw_form)
		{
			$str = substr($arg2_val, 1); // skip '('
			
			$cur = $core->cur_get();
			$core->cur_start($arg2_attr[1],$arg2_attr[2]);
			$str = $core->skip_spaces($str);			
			$rst = '(';
			
			while (strlen($str) > 1 && $str[0] != ')')
			{
				$str = $core->skip_element($str, $ele, $type, $qcnt, $row, $col);
				$str = $core->skip_spaces($str);
				
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}

				$pos = strpos($ele, $having);
				
				if ($pos !== false)
				{
					if (strlen($rst) > 1)
						$rst .= ' ';
					
					$form = array($type, array($qcnt, $row, $col), $ele);
					$rst .= $core->form_to_string($form);
				}
			}
						
			$rst .= ')';
			$core->cur_set($cur);
			
			return array($arg2_type, array($arg2_attr[0], -1, -1), $rst); 
		}
	}
}
?>