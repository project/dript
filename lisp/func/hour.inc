<?php
// Lisp function: hour
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_hour()
{
	return new lisp_func_hour();
}

class lisp_func_hour
{
	function name(&$core)
	{
		return $core->get_func_alias('hour');
	}

	function syntax(&$core)
	{
		return $core->t('(%name {time}) => the hour.' . 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the hour (0 to 23) of time. ' .
						'If time is omitted then the current time will be taken. ' .
						'time can be generated with function (now) or (make-time).');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt >= 4)
		{
			$val = $core->get_value($expr[3][2]);
	
			if (!is_null($val))
				$t = (int)$val;
			else
				$t = time();
		}
		else
			$t = time();

		return array(lisp_eletype_word, array(0, -1, -1), (int)date("G", $t));
	}
}
?>