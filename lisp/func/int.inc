<?php
// Lisp function: int
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_int()
{
	return new lisp_func_int();
}

class lisp_func_int
{
	function name(&$core)
	{
		return $core->get_func_alias('int');
	}

	function syntax(&$core)
	{
		return $core->t('(%name value) => integer value.' . 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Convert a value to integer.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = $core->get_value($expr[3][2]);

		if (is_null($val))
			return;
			
		return array(lisp_eletype_word, array(0, -1, -1), (int)$val);
	}
}
?>