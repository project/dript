<?php
// Lisp function: html-decode
// By Abdullah Daud
// 27 January 2007

function create_lisp_func_html_decode()
{
	return new lisp_func_html_decode();
}

class lisp_func_html_decode
{
	function name(&$core)
	{
		return $core->get_func_alias('html-decode');
	}

	function syntax(&$core)
	{
		return $core->t('(%name string ...) => Concatenated and decoded html string',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Concatenate and decode all html codes: &amp; => &, ' . 
						'&quot; => ", &#039; => \', &lt; => < and &gt; => >. ' .
						'Return concatenated and decoded string.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$str = '';

		for ($i = 3; $i < $cnt; $i++)
		{
			$s = $core->form_to_string($expr[$i]);
			
			if (!$core->is_nil($s)) // Don't include nil
				$str .= $core->strip_dquotes($core->htmlsc_decode($s));
		}

		return array(lisp_eletype_string, array(0, -1, -1), $str);
	}
}
?>