<?php
// Lisp function: is-word
// By Abdullah Daud
// 28 December 2006

function create_lisp_func_is_word()
{
	return new lisp_func_is_word();
}

class lisp_func_is_word
{
	function name(&$core)
	{
		return $core->get_func_alias('is-word');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'ele ...) => T when all elements are words',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Test element for word. Return T when all elements ' . 
						'are words otherwise return nil.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		for ($i = 3; $i < $cnt; $i++)
		{
			if (!$core->is_word($expr[$i][2]))
				return;
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>