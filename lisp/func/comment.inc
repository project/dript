<?php
/**
 * Lisp function: comment
 * By Abduulah Daud
 * 1 January 2007
 */
function create_lisp_func_comment()
{
	return new lisp_func_comment();
}

class lisp_func_comment
{
	function name(&$core)
	{
		return $core->get_func_alias('comment');
	}

	function syntax(&$core)
	{
		return $core->t('(%name comment ... )',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Comment. Do nothing and remove itself.'); 
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function dont_want_nil(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		// Do nothing
	}
}
?>