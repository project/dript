<?php
// Lisp function: cdr
// By Abdullah Daud
// 29 December 2006

function create_lisp_func_cdr()
{
	return new lisp_func_cdr();
}

class lisp_func_cdr
{
	function name(&$core)
	{
		return $core->get_func_alias('cdr');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'(ele ...)) => The list of elements minus the first one',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Remove the first element from the list and return the shortened list.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$arg = $expr[3];
		$arg_type = $arg[0];
		$arg_attr = $arg[1];
		$arg_val = $arg[2];
		
		if ($arg_type == lisp_eletype_form)
		{
			if ($cnt > 3)
			{
				$form = array(lisp_eletype_form, array($arg_attr[0], -1, -1));
				$cnt = count($arg);
			
				for ($i = 3; $i < $cnt; $i++)
				{
					$ele = $arg[$i];
					$form[] = array($ele[0], array($ele[1][0], $ele[1][1], $ele[1][2]), $ele[2]);
				}
			
				return $form;
			}
		}
		else if ($arg_type == lisp_eletype_raw_form)
		{
			$str = substr($arg_val, 1); // skip '('
			
			$cur = $core->cur_get();
			$core->cur_start($arg_attr[1],$arg_attr[2]);
			$str = $core->skip_element($str, $ele, $type, $qcnt, $row, $col);
			$str = $core->skip_spaces($str);
			$core->cur_set($cur);
			
			return array($arg_type, array($arg_attr[0], -1, -1), '(' . $str);
		}
	}
}
?>