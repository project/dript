<?php
// Lisp function: drupal-php-view
// By Abdullah Daud
// 25 January 2007

function create_lisp_func_drupal_php_view()
{
	return new lisp_func_drupal_php_view();
}

class lisp_func_drupal_php_view
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-php-view');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'node-id {\'teaser=nil {\'page=nil [\'links=T]}}) ' . 
						'=> PHP snippet to render view of a node.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get a PHP snippet to render a Drupal\'s node view. ' .
						'node-id may be set to \'this if the function is ' .
						'called from within a node. ' .
						'Enable PHP input format to get a rendered view.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);
		
		if ($cnt < 4)
			return;
			
		$nid_arg = $expr[3][2];

		if (!$core->is_word($nid_arg))
			return;
		
		if ($nid_arg == 'this')
		{
			global $dript_global_var_this_node;
			$node = $dript_global_var_this_node;

			if (!is_object($node))
				return;
		
			$nid = $node->nid;	
		}
		else
		{
			sscanf($nid_arg, "%d", $nid); // retrieve nid from argument
		
			if (!is_int($nid)) // nid must be integer
				return;
		}
					
		$teaser = false;
		$page = false;
		$links = true;
		
		if ($cnt > 4)
		{
			if (!$core->is_nil($expr[4]))
				$teaser = true;
				
			if ($cnt > 5)
			{
				if (!$core->is_nil($expr[5]))
					$page = true;
					
				if ($cnt > 6)
				{
					if ($core->is_nil($expr[6]))
						$links = false;
				}
			}
		}
				
		$t = $teaser ? "true" : "false";			
		$p = $page ? "true" : "false";			
		$l = $links ? "true" : "false";			
		 
		$view = "<?php echo node_view(node_load($nid), $t, $p, $l); ?" . ">";
		return array(lisp_eletype_string, array(0, -1, -1), $view);
	}
}
?>