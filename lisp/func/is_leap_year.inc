<?php
// Lisp function: is-leap-year
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_is_leap_year()
{
	return new lisp_func_is_leap_year();
}

class lisp_func_is_leap_year
{
	function name(&$core)
	{
		return $core->get_func_alias('is-leap-year');
	}

	function syntax(&$core)
	{
		return $core->t('(%name {time}) => T if the time is a leap year.' . 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Check if the time is a leap year. Return T if it is ' .
						'or otherwise return nil. ' .
						'If time is omitted then the current time will be taken. ' .
						'time can be generated with function (now) or (make-time).');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt >= 4)
		{
			$val = $core->get_value($expr[3][2]);
	
			if (!is_null($val))
				$t = (int)$val;
			else
				$t = time();
		}
		else
			$t = time();

		if (date("L", $t) > 0)
			return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>