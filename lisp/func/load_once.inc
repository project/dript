<?php
// Lisp function: load-once
// By Abdullah Daud
// 5 January 2007

function create_lisp_func_load_once()
{
	return new lisp_func_load_once();
}

class lisp_func_load_once
{
	function name(&$core)
	{
		return $core->get_func_alias('load-once');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'filename) => The result of the last expression.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Load an expression file once. ' .
						'The content will be evaluated on the first load. ' .
						'Return the result of the last expression.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$fn = $expr[3][2];

		if (strlen($fn) > 0)
		{
			// If already loaded then return the result of
			// the last expression.	
			if (array_key_exists($fn, $core->loaded_files))
				return $core->loaded_files[$fn];
				
			// First time loading...
			
			$path = $core->user_path . $fn;
			$file_expr = file_get_contents($path);
			
			// Evaluate function expression
				
			$cur = $core->cur_get();
			$core->cur_start(0,0);
			$rst = $core->evaluate($file_expr, $fn, $dre);
			$core->cur_set($cur);
		
			if ($core->is_err())
				return;
			
			$rst = $core->last_ele($rst);
			$core->loaded_files[$fn] = $rst;
			
			return $rst;	
		}
	}
}
?>