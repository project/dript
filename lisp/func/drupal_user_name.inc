<?php
// Lisp function: drupal-user-name
// By Abdullah Daud
// 6 January 2007

function create_lisp_func_drupal_user_name()
{
	return new lisp_func_drupal_user_name();
}

class lisp_func_drupal_user_name
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-user-name');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => The name of the current user.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the name of the current user.'); 
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$user = $GLOBALS['user'];
		
		if (!is_object($user))
			return;
			
		$user_arr = get_object_vars($user);
		
		if (!is_array($user_arr))
			return;
			
		$user_name = $user_arr['name'];
		return array(lisp_eletype_string, array(0, -1, -1), $user_name);
	}
}
?>