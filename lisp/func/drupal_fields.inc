<?php
// Lisp function: drupal-fields
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_drupal_fields()
{
	return new lisp_func_drupal_fields();
}

class lisp_func_drupal_fields
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-fields');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'node-id) => List of field names.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the Drupal\'s node fields. ' .
						'node-id may be set to \'this if the function is ' .
						'called from within a node. ' .
						'Return the list of field names.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		if (count($expr) < 4)
			return;
			
		$nid_arg = $expr[3][2];

		if (!$core->is_word($nid_arg))
			return;
		
		if ($nid_arg == 'this')
		{
			global $dript_global_var_this_node;
			$node = $dript_global_var_this_node;
		}
		else
		{
			sscanf($nid_arg, "%d", $nid); // retrieve nid from argument
		
			if (!is_int($nid)) // nid must be integer
				return;
			
			if (!is_callable('node_load')) // external function check
				return;
			
			$node = node_load($nid); // load Drupal node from database
		}
		
		if (!is_object($node))
			return;
			
		$node_arr = get_object_vars($node);
		
		if (!is_array($node_arr))
			return;
			
		$form = array(lisp_eletype_form, array(0, -1, -1));
			
		foreach ($node_arr as $key => $val)
		{
			$lkey = strtolower($key);
			
			if (substr($lkey, 0, 6) == "field_")
			{
				$name = substr($key, 6);
				$form[] = array(lisp_eletype_string, array(0, -1, -1), $name);
			} 
		}
 
		return $form;
	}
}
?>