<?php
/**
 * Lisp function: eq
 * By Abdullah Daud
 * 28 December 2006
 */
function create_lisp_func_eq()
{
	return new lisp_func_eq();
}

class lisp_func_eq
{
	function name(&$core)
	{
		return $core->get_func_alias('eq');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'value ...) => T when all values are equal',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Equal. If all values are equal then return T otherwise return nil');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);
		$val = 0.0;
						
		for ($i = 3; $i < $cnt; $i++)
		{
			$arg_val = $core->get_value($expr[$i][2]);

			if (!is_null($arg_val))
			{			
				if ($i == 3)
					$val = $arg_val;
				else if ($arg_val != $val)
					return;
			}
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>