<?php
// Lisp function: abs
// By Abdullah Daud
// 11 January 2007

function create_lisp_func_abs()
{
	return new lisp_func_abs();
}

class lisp_func_abs
{
	function name(&$core)
	{
		return $core->get_func_alias('abs');
	}

	function syntax(&$core)
	{
		return $core->t('(%name num ...) => The absolute numbers.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the absolute numbers.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt == 4)
		{
			$ele = $core->get_value($expr[3][2]);

			if (!is_null($ele))
			{
				if ($ele < 0.0)
					$ele = -$ele;
					
				return array(lisp_eletype_word, array(0, -1, -1), $ele);
			}
		}
		
		if ($cnt < 5)
			return;
				
		$form = array(lisp_eletype_form, array(0, -1, -1));
				
		for ($i = 3; $i < $cnt; $i++)
		{
			$ele = $core->get_value($expr[$i][2]);

			if (!is_null($ele))
			{
				if ($ele < 0.0)
					$ele = -$ele;
					
				$form[] = array(lisp_eletype_word, array(0, -1, -1), $ele);
			}
		}

		return $form;		
	}
}
?>