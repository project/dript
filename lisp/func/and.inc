<?php
/**
 * Lisp function: and
 * By Abduulah Daud
 * 27 December 2006
 */
function create_lisp_func_and()
{
	return new lisp_func_and();
}

class lisp_func_and
{
	function name(&$core)
	{
		return $core->get_func_alias('and');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'value ...) => T when all values are non-nil',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('AND operation on all values. If a value is nil then return nil otherwise return T');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);
		
		for ($i = 3; $i < $cnt; $i++)
		{
			if ($core->is_nil($expr[$i][2]))
				return;
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>