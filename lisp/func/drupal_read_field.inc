<?php
// Lisp function: drupal-read-field
// By Abdullah Daud
// 1 January 2007

function create_lisp_func_drupal_read_field()
{
	return new lisp_func_drupal_read_field();
}

class lisp_func_drupal_read_field
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-read-field');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'node-id \'field-name) => values',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Read the value(s) of the Drupal\'s node field. ' .
						'node-id may be set to \'this if the function is ' .
						'called from within a node. ' .
						'Return a value or a list of values.');
	}

	function sub_eval_text(&$core, $text, &$source)
	{
		$xot = $core->output_text;
		$rst = $core->eval_text($text, $source);
		$core->output_text = $xot;
		return $rst;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (count($expr) < 5)
			return;
			
		$nid_arg = $expr[3][2];

		if (!$core->is_word($nid_arg))
			return;
		
		if ($nid_arg == 'this')
		{
			global $dript_global_var_this_node;
			$node = $dript_global_var_this_node;
		}
		else
		{
			sscanf($nid_arg, "%d", $nid); // retrieve nid from argument
		
			if (!is_int($nid)) // nid must be integer
				return;
			
			if (!is_callable('node_load')) // external function check
				return;
			
			$node = node_load($nid); // load Drupal node from database
		}
		
		if (!is_object($node))
			return;
			
		$node_arr = get_object_vars($node);
		
		if (!is_array($node_arr))
			return;
			
		// Retrieve field name and mask it
		$fld_name = $expr[4][2];
		$fld_name = str_replace(' ', '_', $fld_name);
		$fld_name = strtolower($fld_name);
		$fld_name = 'field_' . $fld_name;

		$fld = $node_arr[$fld_name];
		$cnt = count($fld);
 
		if ($cnt <= 0) // field has no value
			return;
 				
 		if ($cnt == 1) // Single value field
			return array(lisp_eletype_string, array(0, -1, -1), 
						 $this->sub_eval_text($core, $fld[0]['value'], $source));
				 	
		// Multiple value field
		
		$form = array(lisp_eletype_form, array(0, -1, -1));
			
		for ($i = 0; $i < $cnt; $i++)
			$form[] = array(lisp_eletype_string, array(0, -1, -1), 
 						 	$this->sub_eval_text($core, $fld[$i]['value'], $source));
			
		return $form;
	}
}
?>