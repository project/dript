<?php
/**
 * Lisp function: eval
 * By Abdullah Daud
 * 30 December 2006
 */
function create_lisp_func_eval()
{
	return new lisp_func_eval();
}

class lisp_func_eval
{
	function name(&$core)
	{
		return $core->get_func_alias('eval');
	}

	function syntax(&$core)
	{
		return $core->t('(%name expr ...) => the result of the last expression',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Evaluate all expressions and return the result of ' . 
						'the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_array($expr))
			return;
			
		$cnt = count($expr);
		
		if ($cnt < 4)
			return;
			
		$last_rst = null;
		$cur = $core->cur_get();
		
		for ($i = 3; $i < $cnt; $i++)
		{
			$c_expr = $expr[$i];
			$c_attr = $expr[$i][1]; 
			$core->cur_start($c_attr[1], $c_attr[2]);
			$e = $core->form_to_string($c_expr);
			$last_rst = $core->evaluate($e, $source, $dre);
		
			if ($core->is_err())
			{
				$core->cur_set($cur);
				return;
			}
		}

		$core->cur_set($cur);

		if ($last_rst != null && is_array($last_rst))
		{
			if ($last_rst[0] == lisp_eletype_form)
				return $last_rst[2];
		}
	}
}
?>