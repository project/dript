<?php
// Lisp function: print
// By Abdullah Daud
// 29 December 2006

function create_lisp_func_print()
{
	return new lisp_func_print();
}

class lisp_func_print
{
	function name(&$core)
	{
		return $core->get_func_alias('print');
	}

	function syntax(&$core)
	{
		return $core->t('(%name string ...) => concatenated string',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Print all strings to output. Return the printed string concatenated.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$str = '';

		for ($i = 3; $i < $cnt; $i++)
		{
			$s = $core->form_to_string($expr[$i]);
			
			if (!$core->is_nil($s)) // Don't print nil
			{
				$s = $core->strip_dquotes($s);
				$core->lisp_print($s);
				$str .= $s;
			}
		}

		return array(lisp_eletype_string, array(0, -1, -1), $str);
	}
}
?>