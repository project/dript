<?php
// Lisp function: drupal-user-email
// By Abdullah Daud
// 6 January 2007

function create_lisp_func_drupal_user_email()
{
	return new lisp_func_drupal_user_email();
}

class lisp_func_drupal_user_email
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-user-email');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => The email of the current user.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the email of the current user.'); 
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$user = $GLOBALS['user'];
		
		if (!is_object($user))
			return;
			
		$user_arr = get_object_vars($user);
		
		if (!is_array($user_arr))
			return;
			
		$user_mail = $user_arr['mail'];
		return array(lisp_eletype_string, array(0, -1, -1), $user_mail);
	}
}
?>