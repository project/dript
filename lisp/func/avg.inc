<?php
// Lisp function: avg
// By Abdullah Daud
// 11 January 2007

function create_lisp_func_avg()
{
	return new lisp_func_avg();
}

class lisp_func_avg
{
	function name(&$core)
	{
		return $core->get_func_alias('avg');
	}

	function syntax(&$core)
	{
		return $core->t('(%name num ...) => The average of numbers.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the average of the given numbers.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;
			
		$total = 0.0;
		$n = 0;
		
		for ($i = 3; $i < $cnt; $i++)
		{
			$ele_arg = $expr[$i];
			
			if ($ele_arg[0] == lisp_eletype_word)
			{
				$ele = strtolower($ele_arg[2]);
				
				if ($core->is_num($ele))
				{
					$total += $core->get_value($ele);
					++$n;					
				}
			}
		}
		
		if ($n > 0)
		{
			$avg = $total / $n;
			return array(lisp_eletype_word, array(0, -1, -1), $avg);
		}
	}
}
?>