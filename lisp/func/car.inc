<?php
// Lisp function: car
// By Abdullah Daud
// 29 December 2006

function create_lisp_func_car()
{
	return new lisp_func_car();
}

class lisp_func_car
{
	function name(&$core)
	{
		return $core->get_func_alias('car');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'(ele ...)) => The first element in the list',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Retrieve the first element from the list.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$arg = $expr[3];
		$arg_type = $arg[0];
		$arg_attr = $arg[1];
		$arg_val = $arg[2];
		
		if ($arg_type == lisp_eletype_form)
		{
			$car = $arg_val;
			$form = array($car[0], array($car[1][0], -1, -1));
			$cnt = count($car);
			
			for ($i = 2; $i < $cnt; $i++)
			{
				$ele = $car[$i];
					
				if (is_array($ele))
					$form[] = array($ele[0], array($ele[1][0], -1, -1), $ele[2]);
				else
					$form[] = $ele;
			}
			
			return $form;
		}
		
		if ($arg_type == lisp_eletype_raw_form)
		{
			$str = substr($arg_val, 1); // skip '('
			
			$cur = $core->cur_get();
			$core->cur_set($arg_attr[1],$arg_attr[2]);
			$core->skip_element($str, $ele, $type, $qcnt, $row, $col);
			$core->cur_set($cur);
			
			return array($type, array($qcnt, $row, $col), $ele);
		}
	}
}
?>