<?php
// Lisp function: is-int
// By Abdullah Daud
// 9 January 2007

function create_lisp_func_is_int()
{
	return new lisp_func_is_int();
}

class lisp_func_is_int
{
	function name(&$core)
	{
		return $core->get_func_alias('is-int');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'ele ...) => T when all elements are integers',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Test element for integer. Return T when all elements ' . 
						'are integers otherwise return nil.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		for ($i = 3; $i < $cnt; $i++)
		{
			if (!$core->is_int($expr[$i][2]))
				return;
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>