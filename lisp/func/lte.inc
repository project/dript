<?php
/**
 * Lisp function: lte
 * By Abduulah Daud
 * 28 December 2006
 */
function create_lisp_func_lte()
{
	return new lisp_func_lte();
}

class lisp_func_lte
{
	function name(&$core)
	{
		return $core->get_func_alias('lte');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'value ...) => T when the first value is less than or equal to the rest of the values',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Less than or equal. If the first value is less than or equal to the rest then return T otherwise return nil');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);
		$val = 0.0;
						
		for ($i = 3; $i < $cnt; $i++)
		{
			$arg_val = $core->get_value($expr[$i][2]);
			
			if (!is_null($arg_val))
			{			
				if ($i == 3)
					$val = $arg_val;
				else if ($val > $arg_val)
					return;
			}
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>