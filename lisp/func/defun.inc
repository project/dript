<?php
// Lisp function: defun
// By Abdullah Daud
// 28 December 2006

function create_lisp_func_defun()
{
	return new lisp_func_defun();
}

class lisp_func_defun
{
	function name(&$core)
	{
		return $core->get_func_alias('defun');
	}

	function syntax(&$core)
	{
		return $core->t('(%name name (arg-var ...) body-expr ...) => name',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Define a function. Argument name will be the function name. ' . 
						'Argument list (arg-var ...) will be the the argument variables ' . 
						'for the function. All variables in the argument are local. ' .
						'And all variables set in the body of the function are local as well. ' .
						'No expression will be evaluated during the function definition. ' . 
						'This function will return the newly defined function name. ' .
						'Call to the newly defined function takes this form: (name arg-var ...). ' .
						'The call to the newly defined function will assign the arg-vars and ' .
						'evaluate the body-expr and ' .
						'will return the result of the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 5)
			return;

		$fname = $expr[3][2];
		
		if (!$core->is_word($fname))
			return;
		
		// Collect arguments
		$args = array();
		$astr = $expr[4][2];
		
		if ($astr[0] == '(')
		{
			$astr = substr($astr, 1);
			$cur = $core->cur_get();
			$core->cur_set($expr[4][1][1], $expr[4][1][2] + 1); // arguments cursor 
			$arg_cur = $core->cur_get();
			
			while (strlen($astr) > 1 && $astr != ')')
			{		
				// Read an element from the list
				$core->cur_set($arg_cur);
				$astr = $core->skip_element($astr, $ele, $type, $qcnt, $row, $col);
				$astr = $core->skip_spaces($astr);
				$arg_cur = $core->cur_get();
			
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}
				
				$args[] = $ele;
			}

			$core->cur_set($cur);			
		}
		
		// Collect body
		$body = array(lisp_eletype_form, array(0, -1, -1));
		
		for ($i = 5; $i < $cnt; $i++)
		{
			$bx = $expr[$i];
			$body[] = array($bx[0], array($bx[1][0], $bx[1][1], $bx[1][2]), $bx[2]);
		}
		
		$core->defun($fname, $args, $body, $source);
		
		return array(lisp_eletype_word, array(0, -1, -1), $fname);
	}
}
?>