<?php
// Lisp function: is-str
// By Abdullah Daud
// 30 December 2006

function create_lisp_func_is_str()
{
	return new lisp_func_is_str();
}

class lisp_func_is_str
{
	function name(&$core)
	{
		return $core->get_func_alias('is-str');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'ele ...) => T when all elements are strings',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Test element for string. Return T when all elements are strings otherwise return nil');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		for ($i = 3; $i < $cnt; $i++)
		{
			$type = $expr[$i][0];
			
			if ($type != lisp_eletype_string &&
			    $type != lisp_eletype_raw_string)
				return;
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>