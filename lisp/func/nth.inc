<?php
// Lisp function: nth
// By Abdullah Daud
// 10 January 2007

function create_lisp_func_nth()
{
	return new lisp_func_nth();
}

class lisp_func_nth
{
	function name(&$core)
	{
		return $core->get_func_alias('nth');
	}

	function syntax(&$core)
	{
		return $core->t('(%name index \'(ele ...)) => The nth element.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the nth element of the list. The first element is ' .
						'index 0.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 5)
			return;

		$index_arg = $expr[3][2];
		
		if (!$core->is_int($index_arg))
			return;
			
		sscanf($index_arg, "%d", $index);			
		
		if ($index < 0)
			return;
			
		$arg = $expr[4];
		$arg_type = $arg[0];
		$arg_attr = $arg[1];
		$arg_val = $arg[2];
		
		if ($arg_type == lisp_eletype_form)
		{
			if ($cnt > 4)
			{
				$cnt = count($arg);
				
				if ($cnt > ($index + 2))
				{
					$ele = $arg[$index + 2];
					
					if ($ele[0] == lisp_eletype_form)
						return $ele;
						
					return array($ele[0], array($ele[1][0], $ele[1][1], $ele[1][2]), $ele[2]);
				}
			}
		}
		else if ($arg_type == lisp_eletype_raw_form)
		{
			$str = substr($arg_val, 1); // skip '('
			$idx = 0;
			
			$cur = $core->cur_get();
			$core->cur_start($arg_attr[1],$arg_attr[2]);
			
			while (strlen($str) > 1 && $str[0] != ')')
			{
				$str = $core->skip_element($str, $ele, $type, $qcnt, $row, $col);
				$str = $core->skip_spaces($str);
				
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}
					
				if ($idx == $index)
				{
					$core->cur_set($cur);
					return array($type, array($qcnt, $row, $col), $ele);
				}
				
				++$idx;
			}
			
			$core->cur_set($cur);
		}
	}
}
?>