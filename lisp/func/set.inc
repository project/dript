<?php
// Lisp function: set
// By Abdullah Daud
// 20 December 2006

function create_lisp_func_set()
{
	return new lisp_func_set();
}

class lisp_func_set
{
	function name(&$core)
	{
		return $core->get_func_alias('set');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'var-name value) => var-name',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Set a variable with a new value');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$var_type = $expr[3][0]; 
		$var_name = $expr[3][2];

		if ($core->is_word($var_name))
		{
			$val_type = $expr[4][0]; 
			$val_qcnt = $expr[4][1][0];
			$val_val = $expr[4][2];
			
			$core->set_var($var_name, $val_type, $val_qcnt, $val_val);
				
			return array($var_type, array(0, -1, -1), $var_name); // return var name
		}
	}
}
?>