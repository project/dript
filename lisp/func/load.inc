<?php
// Lisp function: load
// By Abdullah Daud
// 5 January 2007

function create_lisp_func_load()
{
	return new lisp_func_load();
}

class lisp_func_load
{
	function name(&$core)
	{
		return $core->get_func_alias('load');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'filename) => The result of the last expression.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Load an expression file and evaluate its content. ' .
						'Return the result of the last expression.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$fn = $expr[3][2];

		if (strlen($fn) > 0)
		{
			$path = $core->user_path . $fn;
			$file_expr = file_get_contents($path);
			
			// Evaluate function expression
				
			$cur = $core->cur_get();
			$core->cur_start(0,0);
			$rst = $core->evaluate($file_expr, $fn, $dre);
			$core->cur_set($cur);
		
			if ($core->is_err())
				return;
			
			return $core->last_ele($rst);
		}
	}
}
?>