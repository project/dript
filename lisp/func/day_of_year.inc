<?php
// Lisp function: day-of-year
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_day_of_year()
{
	return new lisp_func_day_of_year();
}

class lisp_func_day_of_year
{
	function name(&$core)
	{
		return $core->get_func_alias('day-of-year');
	}

	function syntax(&$core)
	{
		return $core->t('(%name {time}) => the day of year.' . 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the day (0 to 365) of year of time. ' .
						'If time is omitted then the current time will be taken. ' .
						'time can be generated with function (now) or (make-time).');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt >= 4)
		{
			$val = $core->get_value($expr[3][2]);
	
			if (!is_null($val))
				$t = (int)$val;
			else
				$t = time();
		}
		else
			$t = time();

		return array(lisp_eletype_word, array(0, -1, -1), (int)date("z", $t));
	}
}
?>