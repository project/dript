<?php
// Lisp function: min
// By Abdullah Daud
// 10 January 2007

function create_lisp_func_min()
{
	return new lisp_func_min();
}

class lisp_func_min
{
	function name(&$core)
	{
		return $core->get_func_alias('min');
	}

	function syntax(&$core)
	{
		return $core->t('(%name num ...) => The minimum number.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the minimum number.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;
			
		$nums = array();
		
		for ($i = 3; $i < $cnt; $i++)
		{
			$ele_arg = $expr[$i];
			
			if ($ele_arg[0] == lisp_eletype_word)
			{
				$ele = strtolower($ele_arg[2]);
				
				if ($core->is_num($ele))
					$nums[] = $core->get_value($ele);
			}
		}
		
		if (count($nums) > 0)
		{
			$min = min($nums);
			return array(lisp_eletype_word, array(0, -1, -1), $min);
		}
	}
}
?>