<?php
// Lisp function: drupal-user-id
// By Abdullah Daud
// 6 January 2007

function create_lisp_func_drupal_user_id()
{
	return new lisp_func_drupal_user_id();
}

class lisp_func_drupal_user_id
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-user-id');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => The id of the current user.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the id of the current user.'); 
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$user = $GLOBALS['user'];
		
		if (!is_object($user))
			return;
			
		$user_arr = get_object_vars($user);
		
		if (!is_array($user_arr))
			return;
			
		$user_id = $user_arr['uid'];
		return array(lisp_eletype_word, array(0, -1, -1), $user_id);
	}
}
?>