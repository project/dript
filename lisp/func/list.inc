<?php
// Lisp function: list
// By Abdullah Daud
// 28 December 2006

function create_lisp_func_list()
{
	return new lisp_func_list();
}

class lisp_func_list
{
	function name(&$core)
	{
		return $core->get_func_alias('list');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'ele ...) => (ele ...)',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Return all elements as a list.'); 
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$form = array(lisp_eletype_form, array(0, -1, -1));
		$cnt = count($expr);
		
		for ($i = 3; $i < $cnt; $i++)
		{
			$ele = $expr[$i];
			$form[] = array($ele[0], array($ele[1][0], $ele[1][1], $ele[1][2]), $ele[2]);
		}
		
		return $form;
	}
}
?>