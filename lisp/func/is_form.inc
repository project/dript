<?php
// Lisp function: is-form
// By Abdullah Daud
// 30 December 2006

function create_lisp_func_is_form()
{
	return new lisp_func_is_form();
}

class lisp_func_is_form
{
	function name(&$core)
	{
		return $core->get_func_alias('is-form');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'ele ...) => T when all elements are forms',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Test element for form. Return T when all elements are forms otherwise return nil');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		for ($i = 3; $i < $cnt; $i++)
		{
			$type = $expr[$i][0];
			
			if ($type != lisp_eletype_form &&
			    $type != lisp_eletype_raw_form)
				return;
		}

		return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>