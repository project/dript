<?php
// Lisp function: make-time
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_make_time()
{
	return new lisp_func_make_time();
}

class lisp_func_make_time
{
	function name(&$core)
	{
		return $core->get_func_alias('make-time');
	}

	function syntax(&$core)
	{
		return $core->t('(%name year month day hour minute second) => the time.' . 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Convert the date time to the number of seconds since ' .
						'January 1 1970 00:00:00 GMT.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);
		
		if ($cnt < 9)
			return;

		$arg = array();
		
		for ($i = 3; $i < $cnt; $i++)
		{
			$val = $core->get_value($expr[$i][2]);
	
			if (!is_null($val))
				$arg[$i - 3] = (int)$val;
			else
				$arg[$i - 3] = 0;
		}
		
		$t = mktime($arg[3], $arg[4], $arg[5], $arg[1], $arg[2], $arg[0]);
		return array(lisp_eletype_word, array(0, -1, -1), (int)$t);
	}
}
?>