<?php
// Lisp function: lambda
// By Abdullah Daud
// 2 January 2007

function create_lisp_func_lambda()
{
	return new lisp_func_lambda();
}

class lisp_func_lambda
{
	function name(&$core)
	{
		return $core->get_func_alias('lambda');
	}

	function syntax(&$core)
	{
		return $core->t('((%name (arg-var ...) body-expr ...) arg ...) => ' .
						'The result of the last expression.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Evaluate an in-place function and return the result of ' .
						'the last expression.'); 
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function want_re_eval(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$args = $expr[3][2];
		$body = array(lisp_eletype_form, array(0, -1, -1));
		
		for ($i = 4; $i < $cnt; $i++)
		{
			$bx = $expr[$i];
			$body[] = array($bx[0], array($bx[1][0], $bx[1][1], $bx[1][2]), $bx[2]);
		}
		
		$fname = $core->defun_lambda($args, $body, $source);
		
		return array(lisp_eletype_word, array(0, -1, -1), $fname);
	}
}
?>