<?php
// Lisp function: strf-octal
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_strf_octal()
{
	return new lisp_func_strf_octal();
}

class lisp_func_strf_octal
{
	function name(&$core)
	{
		return $core->get_func_alias('strf-octal');
	}

	function syntax(&$core)
	{
		return $core->t('(%name value {width=0 {zero-padding=nil}}) ' . 
						'=> value as formatted octal string.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Format a value as octal string.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = $core->get_value($expr[3][2]);

		if (is_null($val))
			return;
			
		$wid = 0;
		$zpad = false;
		
		if ($cnt > 4)
		{
			$a = $core->get_value($expr[4][2]);
			
			if (!is_null($a))
				$wid = (int)$a;
				
			if ($cnt > 5)
			{
				if (!$core->is_nil($expr[5]))
					$zpad = true;
			}		
		}	
			
		$fmt = "%";
		
		if ($zpad)
			$fmt .= "0";
			
		$fmt .= $wid . "o";	
		$str = sprintf($fmt, $val);
		return array(lisp_eletype_string, array(0, -1, -1), $str);
	}
}
?>