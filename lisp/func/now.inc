<?php
// Lisp function: now
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_now()
{
	return new lisp_func_now();
}

class lisp_func_now
{
	function name(&$core)
	{
		return $core->get_func_alias('now');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => the time as now.' . 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the time as now. Return the number of seconds since ' .
						'January 1 1970 00:00:00 GMT.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		return array(lisp_eletype_word, array(0, -1, -1), (int)time());
	}
}
?>