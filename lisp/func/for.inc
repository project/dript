<?php
/**
 * Lisp function: for
 * By Abduulah Daud
 * 31 December 2006
 */
function create_lisp_func_for()
{
	return new lisp_func_for();
}

class lisp_func_for
{
	function name(&$core)
	{
		return $core->get_func_alias('for');
	}

	function syntax(&$core)
	{
		return $core->t('(%name (init-expr ctrl-expr cont-expr) expr ...) ' . 
						'=> the result of the last expression',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Assign to the variable each element in the list ' . 
						'and for every assignment evaluate all expressions. ' . 
						'Return the result of the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}

	function _get_ctrl(&$core, &$expr, &$init, &$ctrl, &$cont)
	{
		if (!is_array($expr))
			return false;
			
		$expr_str = $expr[2];
		
		if ($expr_str[0] != '(')
			return false;

		$expr_str = substr($expr_str, 1); // skip '('

		if (strlen($expr_str) < 1 || $expr_str[0] == ')')
			return false;
		
		$cur = $core->cur_get();

		// Retrieve the init expression
		$core->cur_start($expr[1][1], $expr[1][2] + 1);
		$expr_str = $core->skip_element_form($expr_str, $init);
		$expr_str = $core->skip_spaces($expr_str);	
		$expr_cur = $core->cur_get();

		if ($core->is_err() || strlen($expr_str) < 1 || $expr_str[0] == ')')
		{
			$core->cur_set($cur);
			return false;
		}

		// Retrieve the init expression
		$core->cur_set($expr_cur);
		$expr_str = $core->skip_element_form($expr_str, $ctrl);
		$expr_str = $core->skip_spaces($expr_str);	
		$expr_cur = $core->cur_get();

		if ($core->is_err() || strlen($expr_str) < 1 || $expr_str[0] == ')')
		{
			$core->cur_set($cur);
			return false;
		}

		// Retrieve the init expression
		$core->cur_set($expr_cur);
		$expr_str = $core->skip_element_form($expr_str, $cont);

		$core->cur_set($cur);

		return true;			
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_array($expr))
			return;
			
		$cnt = count($expr);
		
		if ($cnt < 4)
			return; // Not enough arguments

		if (!$this->_get_ctrl($core, $expr[3], $init, $ctrl, $cont))
			return;

		$list_cur = $core->cur_get();

		// Evaluate init expression
		$core->cur_start($init[1][1], $init[1][2]);
		$core->evaluate($core->form_to_string($init), $source, $dre);
			
		if ($core->is_err())
		{
			$core->cur_set($cur);
			return;
		}

		$last_expr = null;
		
		while (true)
		{
			// Evaluate ctrl expression
			$core->cur_start($ctrl[1][1], $ctrl[1][2]);
			$rst = $core->evaluate($core->form_to_string($ctrl), $source, $dre);
			
			if ($core->is_err())
			{
				$core->cur_set($cur);
				return;
			}

			if ($core->is_nil($core->last_ele($rst)))
				break;
			
			// Evaluate sub-expressions			
			for ($i = 4; $i < $cnt; $i++)
			{
				$c_expr = $expr[$i];
				$c_attr = $expr[$i][1]; 
				$core->cur_start($c_attr[1], $c_attr[2]);
				$last_rst = $core->evaluate($core->form_to_string($c_expr), $source, $dre);
		
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}
			}
				
			// Evaluate continue expression
			$core->cur_start($cont[1][1], $cont[1][2]);
			$core->evaluate($core->form_to_string($cont), $source, $dre);
			
			if ($core->is_err())
			{
				$core->cur_set($cur);
				return;
			}
		}	

		$core->cur_set($cur);

		if ($last_rst != null && is_array($last_rst))
		{
			if ($last_rst[0] == lisp_eletype_form)
				return $last_rst[2];
		}
	}
}
?>