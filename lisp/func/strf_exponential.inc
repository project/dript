<?php
// Lisp function: strf-exponential
// By Abdullah Daud
// 15 January 2007

function create_lisp_func_strf_exponential()
{
	return new lisp_func_strf_exponential();
}

class lisp_func_strf_exponential
{
	function name(&$core)
	{
		return $core->get_func_alias('strf-exponential');
	}

	function syntax(&$core)
	{
		return $core->t('(%name value) ' . 
						'=> value as formatted exponential string.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Format a value as exponential string.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = $core->get_value($expr[3][2]);

		if (is_null($val))
			return;
			
		$str = sprintf("%e", $val);
		return array(lisp_eletype_string, array(0, -1, -1), $str);
	}
}
?>