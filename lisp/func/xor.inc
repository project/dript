<?php
/**
 * Lisp function: xor
 * By Abduulah Daud
 * 27 December 2006
 */
function create_lisp_func_xor()
{
	return new lisp_func_xor();
}

class lisp_func_xor
{
	function name(&$core)
	{
		return $core->get_func_alias('xor');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'value ...) => T when only one of the values is non-nil',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Exclusive-OR operation on all values. If only one of the value is non-nil then return T otherwise return nil');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);
		$stt = false;
				
		for ($i = 3; $i < $cnt; $i++)
		{
			if (!$core->is_nil($expr[$i][2]))
			{
				if ($stt)
					return;
					
				$stt = true;
			}
		}
		
		if ($stt)
			return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>