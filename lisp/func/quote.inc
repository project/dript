<?php
/**
 * Lisp function: quote
 * By Abduulah Daud
 * 1 January 2007
 */
function create_lisp_func_quote()
{
	return new lisp_func_quote();
}

class lisp_func_quote
{
	function name(&$core)
	{
		return $core->get_func_alias('quote');
	}

	function syntax(&$core)
	{
		return $core->t('(%name ele) => ele',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Return the element without evaluating it.'); 
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		$ele = $expr[3];
		return array($ele[0], array($ele[1][0], $ele[1][1], $ele[1][2]), $ele[2]);
	}
}
?>