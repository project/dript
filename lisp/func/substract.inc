<?php
// Lisp function: substract
// By Abdullah Daud
// 20 December 2006

function create_lisp_func_substract()
{
	return new lisp_func_substract();
}

class lisp_func_substract
{
	function name(&$core)
	{
		return $core->get_func_alias('substract');
	}

	function syntax(&$core)
	{
		return $core->t('(%name value ...) => The first value after being substracted from the rest', 
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Substract the rest of the values from the first value and return the result');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = 0.0;
		$arg_val = $core->get_value($expr[3][2]);

		if (!is_null($arg_val))
			$val = $arg_val;

		for ($i = 4; $i < $cnt; $i++)
		{
			$arg_val = $core->get_value($expr[$i][2]);

			if (!is_null($arg_val))
				$val -= $arg_val;
		}

		return array(lisp_eletype_word, array(0, -1, -1), $val);
	}
}
?>