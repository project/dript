<?php
// Lisp function: substr
// By Abdullah Daud
// 28 December 2006

function create_lisp_func_substr()
{
	return new lisp_func_substr();
}

class lisp_func_substr
{
	function name(&$core)
	{
		return $core->get_func_alias('substr');
	}

	function syntax(&$core)
	{
		return $core->t('(%name string \'start [\'length]) => substring of string',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Substring of string started at position \'start with \'length characters. If \'length is not specified then the substring starts at position \'start till the end of the string.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$s = $core->form_to_string($expr[3]);
			
		if (!$core->is_nil($s)) // Don't include nil
			$str = $core->strip_dquotes($s);
		else
			$str = $s;
		
		if ($cnt >= 5)
			$start = (int)$expr[4][2];
		else
			$start = 0;
			
		if ($cnt >= 6)
		{
			$len = (int)$expr[5][2];
			$substr = substr($str, $start, $len);
		}
		else
			$substr = substr($str, $start);
			
		return array(lisp_eletype_string, array(0, -1, -1), $substr);
	}
}
?>