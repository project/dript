<?php
// Lisp function: after-eval-text
// By Abdullah Daud
// 5 January 2007

function create_lisp_func_after_eval_text()
{
	return new lisp_func_after_eval_text();
}

class lisp_func_after_eval_text
{
	function name(&$core)
	{
		return $core->get_func_alias('after-eval-text');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'func-name) => func-name',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Set the function name that will be called after the ' .
						'API function eval_text() has finished its evaluation on ' .
						'the given text. The function must match the following ' .
						'syntax: (func-name \'text) => text. It will take the ' .
						'processed text from eval_text(). The function will ' .
						'return the same text or an altered text which in turn ' .
						'will be returned by eval_text(). Note: This function ' .
						'is supposed to be called from within the tags inside ' .
						'the text passed to eval_text().');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$fn = $expr[3][2];

		if ($core->is_word($fn))
		{
			$core->after_eval_text_func = $fn;
			return array(lisp_eletype_word, array(0, -1, -1), $fn);
		}
	}
}
?>