<?php
// Lisp function: get
// By Abdullah Daud
// 11 January 2007

function create_lisp_func_get()
{
	return new lisp_func_get();
}

class lisp_func_get
{
	function name(&$core)
	{
		return $core->get_func_alias('get');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'var-name) => var-value',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get a variable\'s value. This function can be used ' .
						'to get an indirect variable value where the unquoted ' .
						'argument variable is earlier set to the name of the ' . 
						'target variable.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$var_type = $expr[3][0]; 
		$var_name = $expr[3][2];

		if ($core->is_word($var_name))
		{
			$val = $core->get_var($var_name, $global);
			
			if (!is_null($val))
				return array($val[0], array($val[1], -1, -1), $val[2]); 
		}
	}
}
?>