<?php
/**
 * Lisp function: not
 * By Abduulah Daud
 * 27 December 2006
 */
function create_lisp_func_not()
{
	return new lisp_func_not();
}

class lisp_func_not
{
	function name(&$core)
	{
		return $core->get_func_alias('not');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'value) => inverted value',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Inverted value. If value is non-nil then return nil otherwise return T');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		if ($core->is_nil($expr[3]))
			return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>