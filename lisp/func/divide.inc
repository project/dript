<?php
// Lisp function: divide
// By Abdullah Daud
// 20 December 2006

function create_lisp_func_divide()
{
	return new lisp_func_divide();
}

class lisp_func_divide
{
	function name(&$core)
	{
		return $core->get_func_alias('divide');
	}

	function syntax(&$core)
	{
		return $core->t('(%name value ...) => The first value after being divided by the rest of the values',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Divide the first value by the rest and return the result');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$val = 0.0;
		$arg_val = $core->get_value($expr[3][2]);

		if (!is_null($arg_val))
			$val = $arg_val;

		for ($i = 4; $i < $cnt; $i++)
		{
			$arg_val = $core->get_value($expr[$i][2]);

			if (!is_null($arg_val))
			{
				if ($arg_val == 0)
				{
					$code->serr(lisp_err_divide_by_zero, $expr[$i][1][1], 
								$expr[$i][1][2], $source);
					return;
				}

				$val /= $arg_val;
			}
		}

		return array(lisp_eletype_word, array(0, -1, -1), $val);
	}
}
?>