<?php
// Lisp function: last
// By Abdullah Daud
// 11 January 2007

function create_lisp_func_last()
{
	return new lisp_func_last();
}

class lisp_func_last
{
	function name(&$core)
	{
		return $core->get_func_alias('last');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'(ele ...)) => The last element.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the last element of the list.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;
		
		$last = null;
		$arg = $expr[3];
		$arg_type = $arg[0];
		$arg_attr = $arg[1];
		$arg_val = $arg[2];
		
		if ($arg_type == lisp_eletype_form)
		{
			if ($cnt > 3)
				$last = $core->last_ele($arg);
		}
		else if ($arg_type == lisp_eletype_raw_form)
		{
			$str = substr($arg_val, 1); // skip '('
			
			$cur = $core->cur_get();
			$core->cur_start($arg_attr[1],$arg_attr[2]);

			$ele = "";
						
			while (strlen($str) > 1 && $str[0] != ')')
			{
				$str = $core->skip_element($str, $ele, $type, $qcnt, $row, $col);
				$str = $core->skip_spaces($str);
				
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}
			}
			
			$core->cur_set($cur);
			
			if (strlen($ele) > 0)
				$last = array($type, array($qcnt, $row, $col), $ele);
		}

		return $last;
	}
}
?>