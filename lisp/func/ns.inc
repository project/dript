<?php
/**
 * Lisp function: ns
 * By Abdullah Daud
 * 25 January 2007
 */
function create_lisp_func_ns()
{
	return new lisp_func_ns();
}

class lisp_func_ns
{
	function name(&$core)
	{
		return $core->get_func_alias('ns');
	}

	function syntax(&$core)
	{
		return $core->t('(%name namespace expr ...) => the result of the last expression',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Bind all expressions to the namespace and evaluate all ' .
						'of them and return the result of the last expression.');
	}

	function dont_eval_args(&$core)
	{
		return true;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_array($expr))
			return;
			
		$cnt = count($expr);
		
		if ($cnt < 5)
			return;
			
		$ns = $expr[3][2];
		$core->ns_begin($ns);
		
		$last_rst = null;
		$cur = $core->cur_get();
		
		for ($i = 4; $i < $cnt; $i++)
		{
			$c_expr = $expr[$i];
			$c_attr = $expr[$i][1]; 
			$core->cur_start($c_attr[1], $c_attr[2]);
			$e = $core->form_to_string($c_expr);
			$last_rst = $core->evaluate($e, $source, $dre);
		
			if ($core->is_err())
			{
				$core->cur_set($cur);
				$core->ns_end($ns);
				return;
			}
		}

		$core->cur_set($cur);
		$core->ns_end($ns);

		if ($last_rst != null && is_array($last_rst))
		{
			if ($last_rst[0] == lisp_eletype_form)
				return $last_rst[2];
		}
	}
}
?>