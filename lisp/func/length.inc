<?php
// Lisp function: length
// By Abdullah Daud
// 10 January 2007

function create_lisp_func_length()
{
	return new lisp_func_length();
}

class lisp_func_length
{
	function name(&$core)
	{
		return $core->get_func_alias('length');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'(ele ...)) => The number of elements.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Get the number of elements in the list.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;
		
		$len = 0;
		$arg = $expr[3];
		$arg_type = $arg[0];
		$arg_attr = $arg[1];
		$arg_val = $arg[2];
		
		if ($arg_type == lisp_eletype_form)
		{
			if ($cnt > 3)
				$len = count($arg) - 2;
		}
		else if ($arg_type == lisp_eletype_raw_form)
		{
			$str = substr($arg_val, 1); // skip '('
			
			$cur = $core->cur_get();
			$core->cur_start($arg_attr[1],$arg_attr[2]);
			
			while (strlen($str) > 1 && $str[0] != ')')
			{
				$str = $core->skip_element($str, $ele, $type, $qcnt, $row, $col);
				$str = $core->skip_spaces($str);
				
				if ($core->is_err())
				{
					$core->cur_set($cur);
					return;
				}
				
				++$len;
			}
			
			$core->cur_set($cur);
		}

		return array(lisp_eletype_word, array(0, -1, -1), $len);
	}
}
?>