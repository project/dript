<?php
// Lisp function: drupal-is-front-page
// By Abdullah Daud
// 6 January 2007

function create_lisp_func_drupal_is_front_page()
{
	return new lisp_func_drupal_is_front_page();
}

class lisp_func_drupal_is_front_page
{
	function name(&$core)
	{
		return $core->get_func_alias('drupal-is-front-page');
	}

	function syntax(&$core)
	{
		return $core->t('(%name) => T when it is the front page.',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Check if the current page is the front page. ' . 
						'Return T if it is.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		if (!is_callable('drupal_is_front_page')) // external function check
				return;
			
		if (drupal_is_front_page())				
			return array(lisp_eletype_word, array(0, -1, -1), 'T');
	}
}
?>