<?php
/**
 * Lisp function: or
 * By Abduulah Daud
 * 27 December 2006
 */
function create_lisp_func_or()
{
	return new lisp_func_or();
}

class lisp_func_or
{
	function name(&$core)
	{
		return $core->get_func_alias('or');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'value ...) => T when any of the values is non-nil',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('OR operation on all values. If a value is non-nil then return T otherwise return nil');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);
		
		for ($i = 3; $i < $cnt; $i++)
		{
			if (!$core->is_nil($expr[$i][2]))
				return array(lisp_eletype_word, array(0, -1, -1), 'T');
		}
	}
}
?>