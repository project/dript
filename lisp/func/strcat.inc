<?php
// Lisp function: strcat
// By Abdullah Daud
// 28 December 2006

function create_lisp_func_strcat()
{
	return new lisp_func_strcat();
}

class lisp_func_strcat
{
	function name(&$core)
	{
		return $core->get_func_alias('strcat');
	}

	function syntax(&$core)
	{
		return $core->t('(%name string ...) => concatenation of all strings',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Concatenate all strings and return the result');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		$str = '';

		for ($i = 3; $i < $cnt; $i++)
		{
			$s = $core->form_to_string($expr[$i]);
			
			if (!$core->is_nil($s)) // Don't include nil
				$str .= $core->strip_dquotes($s);
		}

		return array(lisp_eletype_string, array(0, -1, -1), $str);
	}
}
?>