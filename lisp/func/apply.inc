<?php
// Lisp function: apply
// By Abdullah Daud
// 2 January 2007

function create_lisp_func_apply()
{
	return new lisp_func_apply();
}

class lisp_func_apply
{
	function name(&$core)
	{
		return $core->get_func_alias('apply');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'function \'(ele ...)) => function result',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Apply the function with the element list as the ' .
						'arguments. Return the function result.');
	}

	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 5)
			return;

		$func_name = $expr[3][2];

		if (!$core->is_word($func_name))
			return;

		// Prepare the function arguments
		$arg = $expr[4];
		$arg_type = $arg[0];
		$arg_attr = $arg[1];
		$arg_val = $arg[2];
		
		if ($arg_type == lisp_eletype_form)
		{
			// Prepare the function form
			$func_form = array(lisp_eletype_form, array(0, -1, -1));
			$func_form[] = array(lisp_eletype_word, array(0, -1, -1), $func_name);
		
			$cnt = count($arg);
			
			for ($i = 2; $i < $cnt; $i++)
			{
				$ele = $arg[$i];
				$form_func[] = array($ele[0], array($ele[1][0], $ele[1][1], $ele[1][2]), $ele[2]);
			}
			
			$func_expr = $core->form_to_string($form_func);
		}
		else if ($arg_type == lisp_eletype_raw_form)
		{
			$str = substr($arg_val, 1); // skip '('
			
			$cur = $core->cur_get();
			$core->cur_start($arg_attr[1],$arg_attr[2]);
			$str = $core->skip_spaces($str);
			$core->cur_set($cur);
			
			if (strlen($str) > 1)
				$func_expr = '(' . $core->form_to_string($func_name) . ' ' . $str;
			else
				$func_expr = '(' . $core->form_to_string($func_name) . ')';
		}

		// Evaluate function expression
				
		$cur = $core->cur_get();
		$core->cur_start($arg_attr[1],$arg_attr[2]);
		$rst = $core->evaluate($func_expr, $source, $dre);
		$core->cur_set($cur);
		
		if ($core->is_err())
			return;
			
		return $rst[2];	
	}
}
?>