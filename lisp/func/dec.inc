<?php
// Lisp function: dec
// By Abdullah Daud
// 30 December 2006

function create_lisp_func_dec()
{
	return new lisp_func_dec();
}

class lisp_func_dec
{
	function name(&$core)
	{
		return $core->get_func_alias('dec');
	}

	function syntax(&$core)
	{
		return $core->t('(%name \'var ...) => var-1 ...',
						array('%name' => $this->name($core)));
	}

	function description(&$core)
	{
		return $core->t('Decrement each variable by one. Return the new values.');
	}

	function _inc(&$core, $var_name)
	{
		if ($core->is_word($var_name))
		{
			$global = false;
			$var = $core->get_var($var_name, $global);
			
			if ($var != null && $var[0] == lisp_eletype_word && $var[1] == 0)
			{
				if ($core->is_word($var[2]))
				{
					$val_form = array($var[0], array($var[1], -1, -1), $var[2]);
					$val_str = $core->form_to_string($val_form);
					$v = $core->get_value($val_str);
					$v -= 1.0;
				
					if ($global)
						$core->set_global_var($var_name, $var[0], $var[1], $v);
					else
						$core->set_local_var($var_name, $var[0], $var[1], $v);
				
					return $v;
				}
			}
		}
		
		return 0.0;
	}
	
	function evaluate(&$core, &$expr, &$source)
	{
		$cnt = count($expr);

		if ($cnt < 4)
			return;

		if ($cnt == 4)
		{
			$val = $this->_inc($core, $expr[3][2]);
			return array(lisp_eletype_word, array(0, -1, -1), $val);
		}
		
		$form = array(lisp_eletype_form, array(0, -1, -1));
		
		for ($i = 3; $i < $cnt; $i++)
		{
			$val = $this->_inc($core, $expr[$i][2]);
			$form[] = array(lisp_eletype_word, array(0, -1, -1), $val);
		}

		return $form;
	}
}
?>